package main

import (
	"fmt"
	"math/rand"
	"time"
)

var (
	Web   = fakeSearch("web")
	Image = fakeSearch("image")
	Video = fakeSearch("video")
)

type Result string

type Search func(query string) Result

func fakeSearch(kind string) Search {
	return func(query string) Result {
		time.Sleep(time.Duration(rand.Intn(100)) * time.Millisecond)
		return Result(fmt.Sprintf("%s result for %q\n", kind, query))
	}
}

func Googlev1(query string) (results []Result) {
	results = append(results, Web(query))
	results = append(results, Image(query))
	results = append(results, Video(query))
	return
}

func Googlev2(query string) (results []Result) {
	c := make(chan Result, 3)

	go func() { c <- Web(query) }()
	go func() { c <- Image(query) }()
	go func() { c <- Video(query) }()

	for i := 0; i < 3; i++ {
		results = append(results, <-c)
	}

	return
}

func Googlev2_1(query string) (results []Result) {
	c := make(chan Result, 3)

	go func() { c <- Web(query) }()
	go func() { c <- Image(query) }()
	go func() { c <- Video(query) }()

	timeout := time.After(80 * time.Millisecond)
	for i := 0; i < 3; i++ {
		select {
		case x := <-c:
			results = append(results, x)
		case <-timeout:
			fmt.Println("=== timeout ===")
			return
		}
	}

	return
}

func FirstResult(query string, replicas ...Search) Result {
	c := make(chan Result)
	for i := range replicas {
		go func(i int) {
			c <- replicas[i](query)
		}(i)
	}

	return <-c
}

var (
	Web1 = fakeSearch("web1")
	Web2 = fakeSearch("web2")

	Image1 = fakeSearch("image1")
	Image2 = fakeSearch("image2")

	Video1 = fakeSearch("video1")
	Video2 = fakeSearch("video2")
)

func Googlev3(query string) (results []Result) {
	c := make(chan Result, 3)

	go func() { c <- FirstResult(query, Web1, Web2) }()
	go func() { c <- FirstResult(query, Image1, Image2) }()
	go func() { c <- FirstResult(query, Video1, Video2, fakeSearch("video3")) }()

	timeout := time.After(80 * time.Millisecond)
	for i := 0; i < 3; i++ {
		select {
		case x := <-c:
			results = append(results, x)
		case <-timeout:
			fmt.Println("=== timeout ===")
			return
		}
	}

	return
}

func main() {
	rand.Seed(time.Now().UnixNano())
	start := time.Now()
	results := Googlev3("golang")
	elapsed := time.Since(start)
	fmt.Println(results)
	fmt.Println(elapsed)
}
