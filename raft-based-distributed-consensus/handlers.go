package main

import (
	"context"
	"fmt"
	"log/slog"
	"raft-based-distributed-consensus/pb"
	"sync"

	"google.golang.org/grpc"

	"github.com/dyweb/gommon/errors"
)

// initializeRaftNode initializes a RaftNode based on the provided configuration.
func initializeRaftNode(conf *Config) *RaftNode {
	raftNode := &RaftNode{
		CurrentTerm:       0,
		Type:              TypeFollower,
		Leader:            0,
		VotedFor:          0,
		ID:                conf.Info.ID,
		ElectionTimeout:   conf.ElectionTimeout,
		HeartbeatInterval: conf.HeartbeatInterval,
	}
	raftNode.ClusterInfo = make([]ConfigNodeInfo, len(conf.ClusterInfo))
	_ = copy(raftNode.ClusterInfo, conf.ClusterInfo)
	return raftNode
}

func (raftService *RaftService) currTerm() uint64 {
	raftService.mx.Lock()
	defer raftService.mx.Unlock()

	return raftService.raftNode.CurrentTerm
}

func (raftService *RaftService) incTerm() uint64 {
	raftService.mx.Lock()
	defer raftService.mx.Unlock()

	raftService.raftNode.CurrentTerm++
	raftService.raftNode.VotedFor = 0 // reset any votes
	return raftService.raftNode.CurrentTerm
}

func (raftService *RaftService) setRoleOfNode(nodeType NodeType) {
	raftService.mx.Lock()
	defer raftService.mx.Unlock()

	raftService.raftNode.Type = nodeType
}

func (raftService *RaftService) getRoleOfNode() NodeType {
	raftService.mx.Lock()
	defer raftService.mx.Unlock()

	return raftService.raftNode.Type
}

func (raftService *RaftService) isLeader() (string, bool) {
	raftService.mx.Lock()
	defer raftService.mx.Unlock()

	return string(raftService.raftNode.Type), raftService.raftNode.Type == TypeLeader
}

// func (raftService *RaftService) getLeader() (uint64, bool) {
// 	raftService.mx.Lock()
// 	defer raftService.mx.Unlock()
//
// 	if raftService.raftNode.Leader == raftService.raftNode.ID {
// 		return raftService.raftNode.Leader, true
// 	}
// 	return raftService.raftNode.Leader, true
// }

func (raftService *RaftService) upgradeSelfToLeader() {
	raftService.mx.Lock()
	defer raftService.mx.Unlock()

	raftService.raftNode.Type = TypeLeader
	raftService.raftNode.Leader = raftService.raftNode.ID

	slog.Info("leader-candidate proposal successful; promoted self to leader", "node_id", raftService.raftNode.ID)
}

func (raftService *RaftService) currLeader() uint64 {
	raftService.mx.Lock()
	defer raftService.mx.Unlock()

	return raftService.raftNode.Leader
}

func (raftService *RaftService) leaderHeartbeatTimeoutErrorFunc(err error) {
	raftService.mx.Lock()
	defer raftService.mx.Unlock()

	raftService.raftNode.Type = TypeFollower

	slog.Warn("error encountered on task leader_heartbeat_timeout", "error", err)
}

// HeartbeatFunc used it
func (raftService *RaftService) HeartbeatFunc(ctx context.Context) error {
	var wg sync.WaitGroup
	merr := errors.NewMultiErrSafe()
	term := raftService.currTerm()

	id := raftService.raftNode.ID

	slog.Info("triggered leader-heartbeat-timeout scheduled task; proposing self as candidate", "node_id", id, "term", term)

	wg.Add(len(raftService.raftNode.ClusterInfo))

	for _, peer := range raftService.raftNode.ClusterInfo {
		go func(peer ConfigNodeInfo) {
			defer wg.Done()
			if peer.ID == id {
				return
			}

			err := raftService.sendHeartbeatToPeer(ctx, peer.Addr, term, id)
			if err != nil {
				merr.Append(err)
			}
		}(peer)
	}

	wg.Wait()
	if merr.HasError() && merr.Len() > len(raftService.raftNode.ClusterInfo)/2 {
		slog.Error("error encountered when sending heartbeat to peers", "error", merr, "term", term)
		return merr
	}

	return nil
}

func (raftService *RaftService) sendHeartbeatToPeer(ctx context.Context, peer string, term, id uint64) (err error) {

	client, err := NewRaftClient(peer)
	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			slog.Error("Error closing the client rpc", "err", err)
		}
	}(client.conn)
	if err != nil {
		return
	}

	entries, err := client.client.AppendEntries(ctx, &pb.AppendEntriesReq{
		Term:     term,
		LeaderId: id,
	})
	if err != nil {
		return err
	}
	if !entries.Success {
		return fmt.Errorf("leader heartbeat not acknowledged by peer: '%s' for term %d", peer, term)
	}
	slog.Info("returned", "AppendEntries RPC response", entries)
	return nil
}

// LeaderHeartbeatTimeoutFunc use it
func (raftService *RaftService) LeaderHeartbeatTimeoutFunc(ctx context.Context) error {
	var (
		wg   sync.WaitGroup
		merr = errors.NewMultiErrSafe()
		term = raftService.incTerm()
	)
	id := raftService.raftNode.ID
	slog.Info("triggered leader-heartbeat-timeout scheduled task; proposing self as candidate", "node_id", raftService.raftNode.ID, "term", term)

	wg.Add(len(raftService.raftNode.ClusterInfo))

	for _, peer := range raftService.raftNode.ClusterInfo {
		go func(peer ConfigNodeInfo) {
			defer wg.Done()
			if peer.ID == id { // avoid sending it to itself it votes for itself
				return
			}
			err := raftService.sendCandidateProposalToPeer(ctx, peer.Addr, term, id)
			if err != nil {
				merr.Append(err)
			}

		}(peer)
	}

	wg.Wait()
	if merr.HasError() && merr.Len() > len(raftService.raftNode.ClusterInfo)/2 {
		slog.Warn("error encountered when sending candidate proposal to peers", "error", merr, "term", term)
		return merr
	}

	raftService.upgradeSelfToLeader()

	return nil
}

func (raftService *RaftService) sendCandidateProposalToPeer(ctx context.Context, peer string, term, id uint64) (err error) {
	client, err := NewRaftClient(peer)
	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			slog.Error("Error closing the client rpc", "err", err)
		}
	}(client.conn)
	if err != nil {
		return
	}

	vote, err := client.client.RequestVote(ctx, &pb.RequestVoteReq{
		Term:        term,
		CandidateId: id,
	})
	if err != nil {
		return err
	}
	if !vote.VoteGranted {
		return fmt.Errorf("failed to accept proposal from peer '%s' for term %d", peer, term)
	}
	slog.Info("returned", "RequestVote RPC response", vote)
	return nil
}
