package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/fatih/color"
	"google.golang.org/grpc/credentials/insecure"
	"log/slog"
	"net"
	pb_client "raft-based-distributed-consensus/pb-client"
	"strings"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
)

func (s *RaftClientServer) HTTPServerLoadData(conf *Config) {
	s.metadataIPs = make(map[uint64]string)
	for _, peer := range conf.ClusterInfo {
		s.metadataIPs[peer.ID] = peer.Client
	}
}

var (
	HEALTHY   = color.HiGreenString("HEALTHY")
	UNHEALTHY = color.HiRedString("UNHEALTHY")
)

type RPCClient struct {
	conn   *grpc.ClientConn
	client pb_client.RaftClientClient
}

func NewRPCClient(addr string) (*RPCClient, error) {

	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	}

	conn, err := grpc.Dial(addr, opts...)
	if err != nil {
		return nil, err
	}

	return &RPCClient{
		conn:   conn,
		client: pb_client.NewRaftClientClient(conn),
	}, nil
}

// set key=value
// get key

// SendCommand implements the SendCommand RPC method.
func (s *RaftClientServer) SendCommand(ctx context.Context, req *pb_client.SendCommandRequest) (*pb_client.SendCommandResponse, error) {
	raw := strings.Split(req.Command, " ")
	slog.Info("client command", "raw", raw)

	res := &pb_client.SendCommandResponse{}

	switch raw[0] {
	case "health":
		return &pb_client.SendCommandResponse{Response: "{'message': 'server is healthy'}"}, nil
	case "leader":
		if raftService.getRoleOfNode() == TypeLeader {
			res.Leader = true
			res.Response = fmt.Sprintf("{'message': 'Hi node_id: %d is leader of term: %d'", raftService.raftNode.ID, raftService.currTerm())

		} else {
			raftService.mx.Lock()
			leaderID := raftService.raftNode.Leader
			raftService.mx.Unlock()

			var addr string
			addr = s.metadataIPs[leaderID]
			if leaderID == 0 {
				addr = "127.0.0.1:8080"
			}
			res.Response = fmt.Sprintf("{'message': 'node_id: %d is the leader of term: %d'", raftService.currLeader(), raftService.currTerm())

			res.LeaderAddress = addr
		}
	case "members":
		// only fetch from the member leader of that term
		// if its leader it will return a string with specific format
		// where it contains the member id with addresses
		// and now its client rpc to determine its health status to before showing the user

		if raftService.getRoleOfNode() != TypeLeader {
			raftService.mx.Lock()
			leaderID := raftService.raftNode.Leader
			raftService.mx.Unlock()

			addr := s.metadataIPs[leaderID]
			if leaderID == 0 {
				addr = "127.0.0.1:8080"
			}
			res.Response = fmt.Sprintf("{'message': 'node_id: %d is the leader of term: %d'", raftService.currLeader(), raftService.currTerm())

			res.LeaderAddress = addr
		} else {
			type info struct {
				ID     uint64 `json:"id"`
				Addr   string `json:"peer_address"`
				Client string `json:"client_address"`
				Status string `json:"status"`
			}
			var data []info
			for idx, clusterInfo := range raftService.raftNode.ClusterInfo {
				data = append(data, info{
					ID:     clusterInfo.ID,
					Status: HEALTHY,
					Addr:   clusterInfo.Addr,
					Client: clusterInfo.Client})

				// we can do much better by using grpc client to call other member for their health check
				// if its successful then add then with status healthy and otherwise unhealthy
				client, err := NewRPCClient(clusterInfo.Client)
				if err != nil {
					data[idx].Status = UNHEALTHY
					slog.Error(err.Error())
					continue
				}

				_, err = client.client.SendCommand(ctx, &pb_client.SendCommandRequest{Command: "health"})
				if err != nil {
					data[idx].Status = UNHEALTHY
				}
			}

			raw, err := json.MarshalIndent(data, "", " ")
			if err != nil {
				return nil, status.Error(codes.Unknown, err.Error())
			}

			return &pb_client.SendCommandResponse{Response: string(raw), Leader: true}, nil
		}
	case "set":
		if raftService.getRoleOfNode() != TypeLeader {
			raftService.mx.Lock()
			leaderID := raftService.raftNode.Leader
			raftService.mx.Unlock()

			addr := s.metadataIPs[leaderID]
			if leaderID == 0 {
				addr = "127.0.0.1:8080"
			}
			res.Response = fmt.Sprintf("{'message': 'node_id: %d is the leader of term: %d'", raftService.currLeader(), raftService.currTerm())

			res.LeaderAddress = addr
		} else {
			res.Leader = true
			raftService.mx.Lock()
			defer raftService.mx.Unlock()
			///// in the above 2 lines we are trying to lock the election to priorityzing data

			data := strings.Split(raw[1], "=")
			payload := DataStruct{Key: data[0], Value: data[1]}
			mgdb, err := NewClient(ctx)
			if err != nil {
				return nil, status.Error(codes.Internal, err.Error())
			}
			if err := mgdb.Write(payload); err != nil {
				return nil, status.Error(codes.Unknown, err.Error())
			}
			res.Response = "[OK] inserted"
		}

	case "get":
		if raftService.getRoleOfNode() != TypeLeader {
			raftService.mx.Lock()
			leaderID := raftService.raftNode.Leader
			raftService.mx.Unlock()

			addr := s.metadataIPs[leaderID]
			if leaderID == 0 {
				addr = "127.0.0.1:8080"
			}
			res.Response = fmt.Sprintf("{'message': 'node_id: %d is the leader of term: %d'", raftService.currLeader(), raftService.currTerm())

			res.LeaderAddress = addr
		} else {
			res.Leader = true
			raftService.mx.Lock()
			defer raftService.mx.Unlock()
			///// in the above 2 lines we are trying to lock the election to priorityzing data

			key := raw[1]
			mgdb, err := NewClient(ctx)
			if err != nil {
				return nil, status.Error(codes.Internal, err.Error())
			}
			if v, err := mgdb.Read(key); err != nil {
				return nil, status.Error(codes.Unknown, err.Error())
			} else {
				res.Response = fmt.Sprintf("[OK] Data = %#v", v)
			}

		}

	case "del":
		if raftService.getRoleOfNode() != TypeLeader {
			raftService.mx.Lock()
			leaderID := raftService.raftNode.Leader
			raftService.mx.Unlock()

			addr := s.metadataIPs[leaderID]
			if leaderID == 0 {
				addr = "127.0.0.1:8080"
			}
			res.Response = fmt.Sprintf("{'message': 'node_id: %d is the leader of term: %d'", raftService.currLeader(), raftService.currTerm())

			res.LeaderAddress = addr
		} else {
			res.Leader = true
			raftService.mx.Lock()
			defer raftService.mx.Unlock()
			///// in the above 2 lines we are trying to lock the election to priorityzing data

			key := raw[1]
			mgdb, err := NewClient(ctx)
			if err != nil {
				return nil, status.Error(codes.Internal, err.Error())
			}
			if err := mgdb.Delete(key); err != nil {
				return nil, status.Error(codes.Unavailable, err.Error())
			}
			res.Response = "[OK] deleted"
		}

	default:
		return nil, status.Error(codes.InvalidArgument, "no command matched!!! only available are [health, leader]")
	}

	return res, nil
}

// startGRPCServer starts the gRPC server for the Raft node.
func startGRPCClient(client *RaftClientServer, address string) (err error) {
	client.lis, err = net.Listen("tcp", address)
	if err != nil {
		err = fmt.Errorf("unable to set up listener. Reason: %w", err)
		return
	}

	client.server = grpc.NewServer()
	reflection.Register(client.server) // for debugging purposes

	pb_client.RegisterRaftClientServer(client.server, client)

	return
}
