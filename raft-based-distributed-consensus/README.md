---
Author: Dipankar Das
Date: 2023-12-24
---

# Raft based Distributed Consensus enabled datastore

it demonstrates the use of distributed consensus model. Using Raft developed the node with peer and client connections with GRPC.
And using the cli client you can save key=value data in the raft model
also under the hood it helps to read,write,delete data from the external database through leader of the raft nodes

## How to run

### Prequisites
- Go
- Linux, MacOS

### Running the raft nodes
for custom configurations you can add them to `config/`
```bash
go run -race . -config config/node_1.yaml
go run -race . -config config/node_2.yaml
go run -race . -config config/node_3.yaml
```

for more detailed view
```bash
# add
export NODE_INFO="true" # before running the above commands
# for connecting the Database from the Raft nodes you need to specify the URI

# in my case it was like this
export MONGODB_URI="mongodb+srv://dipankar:<password>@cluster0.jz2jx2x.mongodb.net/?retryWrites=true&w=majority"
```

for the cli client
```bash
make build_cli

./cli health --endpoint 127.0.0.1:<>
./cli leader --endpoint 127.0.0.1:<>

# for setting value, getting, deleting
./cli set --data "year=2023" --endpoint 127.0.0.1:<>
./cli get --data "year" --endpoint 127.0.0.1:<>
./cli del --data "year" --endpoint 127.0.0.1:<>

# the data is in format <key>=<value>
```

> also the cli works by first calling to the endpoint specified and if it failed due to it being not a leader.
It will make call to the leaderNode of that term

## Some Snapshots taken 📸

### Health and Current Leader
![](./img/health.png)

### Write data
![](./img/write.png)

### Read data
![](./img/read.png)

### Delete data
![](./img/delete.png)

### Members
![](./img/members.png)

## for profiling task (debugging)

add
```go
import _ "net/http/pprof"

///.....
go func() {
    log.Println(http.ListenAndServe("localhost:6060", nil))
}()

```

```bash
go tool pprof -http=:8080 http://localhost:6060/debug/pprof/profile
```

## References
- [WhitePaper](https://raft.github.io/raft.pdf)
