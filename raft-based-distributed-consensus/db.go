package main

import (
	"context"
	"errors"
	"fmt"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	mongoOptions "go.mongodb.org/mongo-driver/mongo/options"
)

type MongoServer struct {
	client         *mongo.Client
	context        context.Context
	databaseClient *mongo.Database
}

type DataStruct struct {
	Key   string `json:"key" bson:"key"`
	Value any    `json:"value" bson:"value"`
}

func NewClient(ctx context.Context) (*MongoServer, error) {
	client := &MongoServer{context: context.Background()}

	opts := mongoOptions.Client().SetTimeout(time.Minute).ApplyURI(os.Getenv("DB_URI"))

	mongoClient, err := mongo.Connect(client.context, opts)
	if err != nil {
		return nil, fmt.Errorf("MongoDB failed to connect. Reason: %w", err)
	}

	if err := mongoClient.Database("admin").RunCommand(client.context, bson.D{{"ping", 1}}).Err(); err != nil {
		return nil, err
	}

	fmt.Println("Pinged your deployment. You successfully connected to MongoDB!")

	client.databaseClient = mongoClient.Database("raft")
	client.client = mongoClient

	return client, nil
}

func (db *MongoServer) IsPresent(key string) bool {

	c, err := db.databaseClient.Collection("db").Find(db.context, bson.M{"key": key})
	return err != mongo.ErrNoDocuments && c.RemainingBatchLength() == 1
}

func (db *MongoServer) Write(data DataStruct) error {
	bsonMap, err := bson.Marshal(data)
	if err != nil {
		return err
	}

	if db.IsPresent(data.Key) {
		res := db.databaseClient.Collection("db").FindOneAndReplace(db.context, bson.M{"key": data.Key}, bsonMap)
		if err := res.Err(); err != nil {
			return err
		}
		return nil
	}

	_, err = db.databaseClient.Collection("db").InsertOne(db.context, bsonMap)
	return err
}

func (db *MongoServer) Read(key string) (DataStruct, error) {
	ret := db.databaseClient.Collection("db").FindOne(db.context, bson.M{"key": key})
	var result DataStruct
	err := ret.Decode(&result)
	if err != nil {
		return DataStruct{}, err
	}

	return result, nil
}

func (db *MongoServer) Delete(key string) error {
	ret, err := db.databaseClient.Collection("db").DeleteOne(db.context, bson.M{"key": key})
	if err != nil {
		return err
	}

	if ret.DeletedCount == 0 {
		return errors.New("no records deleted")
	}

	fmt.Println("Deleted no of records:", ret.DeletedCount)

	return nil
}
