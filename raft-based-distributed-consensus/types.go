package main

import (
	"net"
	"raft-based-distributed-consensus/pb"
	pb_client "raft-based-distributed-consensus/pb-client"
	"sync"
	"time"

	"google.golang.org/grpc"
)

type NodeType string

const (
	TypeLeader    NodeType = "leader"
	TypeCandidate NodeType = "candidate"
	TypeFollower  NodeType = "follower"
)

type ConfigNodeInfo struct {
	ID     uint64 `yaml:"id"`
	Addr   string `yaml:"peer_address"`
	Client string `yaml:"client_address"`
}

type Config struct {
	Info              ConfigNodeInfo   `yaml:"info"`
	HeartbeatInterval time.Duration    `yaml:"heartbeat_interval"`
	ElectionTimeout   time.Duration    `yaml:"election_timeout"`
	ClusterInfo       []ConfigNodeInfo `yaml:"cluster_info"`
}

type RaftNode struct {
	CurrentTerm       uint64
	VotedFor          uint64
	Type              NodeType
	HeartbeatInterval time.Duration
	ElectionTimeout   time.Duration
	ID                uint64
	ClusterInfo       []ConfigNodeInfo
	Leader            uint64
}

type RaftService struct {
	raftNode *RaftNode
	server   *RaftServer
	mx       sync.Mutex
	pb.UnimplementedPeerConnServer
}

type RaftClientServer struct {
	server      *grpc.Server
	lis         net.Listener
	metadataIPs map[uint64]string

	pb_client.UnimplementedRaftClientServer
}

type RaftServer struct {
	srv    pb.PeerConnServer
	server *grpc.Server
	lis    net.Listener
}

type RaftClient struct {
	conn   *grpc.ClientConn
	client pb.PeerConnClient
}
