package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"net"
	"os"
	"os/signal"
	"raft-based-distributed-consensus/pb"
	"strings"
	"syscall"
	"time"

	"log/slog"

	"github.com/fatih/color"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	yaml "gopkg.in/yaml.v3"
)

const (
	updateInterval = time.Duration(5 * time.Second)
)

var (
	raftService      *RaftService      = &RaftService{server: &RaftServer{}}
	raftClientServer *RaftClientServer = &RaftClientServer{}

	// clientToPeer chan string = make(chan string, 1)
)

func GetConfig() (conf *Config, err error) {
	configPath := flag.String("config", "", "file name as payload")

	// Parse the command-line flags
	flag.Parse()

	// Check if required arguments are provided
	if *configPath == "" {
		err = errors.New("usage: go run main.go -config <value>")
		slog.Error(err.Error())
		return
	}
	raw, err := os.ReadFile(*configPath)
	if err != nil {
		return
	}
	err = yaml.Unmarshal(raw, &conf)
	return
}

func main() {
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)

	// Load configuration
	conf, err := GetConfig()
	if err != nil {
		slog.Error("[FATAL]", "err", err)
		os.Exit(1)
	}

	raftClientServer.HTTPServerLoadData(conf)

	// Initialize Raft Node
	raftService.raftNode = initializeRaftNode(conf)
	fmt.Printf("Initialized Raft Node: %#+v\n", raftService.raftNode)

	// Start gRPC server
	err = startGRPCServer(raftService, conf.Info.Addr)
	if err != nil {
		slog.Error("[FATAL]", "err", err)
		os.Exit(1)
	}

	err = startGRPCClient(raftClientServer, conf.Info.Client)
	if err != nil {
		slog.Error("[FATAL]", "err", err)
		os.Exit(1)
	}

	onlyRunPeerRPC := make(chan bool)
	onlyRunClientRPC := make(chan bool)

	go func() {
		onlyRunPeerRPC <- true
		onlyRunClientRPC <- true
	}()

	var printNodeInfo = os.Getenv("NODE_INFO")

	heart := raftService.raftNode.HeartbeatInterval
	election := raftService.raftNode.ElectionTimeout

	var nextHeart, nextElect time.Time

	nextElect = time.Now().Add(election)

	ticker := time.NewTicker(updateInterval)

	for {
		var fetchDelayHeart, fetchDelayElect time.Duration
		if now := time.Now(); nextHeart.After(now) {
			fetchDelayHeart = nextHeart.Sub(now)
		}

		heartInterv := time.After(fetchDelayHeart)

		if now := time.Now(); nextElect.After(now) {
			fetchDelayElect = nextElect.Sub(now)
		}

		electionInterv := time.After(fetchDelayElect)

		select {
		case <-ctx.Done():
			// Wait for termination signal
			slog.Info("Received termination signal. Shutting down...")

			// Graceful shutdown
			stopGRPCServer(raftService)
			return

		case <-onlyRunPeerRPC:
			slog.Info("Server for Peer RPC starting", "address", conf.Info.Addr)

			go func() {
				if err := raftService.server.server.Serve(raftService.server.lis); err != nil {
					slog.Error("Failed to serve", "err", err)
				}
				close(onlyRunPeerRPC)
				stop()
			}()

		case <-onlyRunClientRPC:
			slog.Info("Server for Client RPC starting", "address", conf.Info.Client)

			go func() {
				if err := raftClientServer.server.Serve(raftClientServer.lis); err != nil {
					slog.Error("Failed to serve", "err", err)
				}
				close(onlyRunClientRPC)
				stop()
			}()

		case <-ticker.C:
			if printNodeInfo == "true" {

				nodetype := raftService.getRoleOfNode()
				leader := raftService.currLeader()
				term := raftService.currTerm()

				var rawStr strings.Builder
				rawStr.WriteString("$$$$$$$$$ NODE_INFO ~5s $$$$$$$\n")
				rawStr.WriteString(color.YellowString("> NODE_ID: %v\n", conf.Info.ID))
				rawStr.WriteString(color.BlueString("> TERM: %v\n", term))
				rawStr.WriteString(color.HiGreenString("> NODE_ROLE: %s\n", (nodetype)))
				rawStr.WriteString(color.RedString("> TERM_LEADER: %v\n", leader))
				rawStr.WriteString("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n")
				fmt.Println(rawStr.String())
			}

		case <-heartInterv:
			if _, ok := raftService.isLeader(); !ok {
				nextHeart = time.Now().Add(heart + heart)
				continue
			}

			slog.Info("HeartBeat started", "term", raftService.currTerm(), "node_id", raftService.raftNode.ID)

			err := raftService.HeartbeatFunc(ctx)

			if err != nil {
				slog.Warn("Error in heartbeat", "err", err)
			} else {
				slog.Info("==== LEADER ====", "term", raftService.raftNode.CurrentTerm, "leader_id", raftService.raftNode.Leader)
				nextElect = time.Now().Add(election)
			}
			nextHeart = time.Now().Add(heart)

		case <-electionInterv:
			slog.Info("it's election time", "term", raftService.currTerm(), "node_id", raftService.raftNode.ID)

			raftService.setRoleOfNode(TypeCandidate)

			err := raftService.LeaderHeartbeatTimeoutFunc(ctx)
			if err != nil {
				raftService.leaderHeartbeatTimeoutErrorFunc(err)
			} else {
				slog.Info("==== LEADER ====", "term", raftService.raftNode.CurrentTerm, "leader_id", raftService.raftNode.Leader)
				err := raftService.HeartbeatFunc(ctx)

				if err != nil {
					slog.Warn("Error in heartbeat", "err", err)
				} else {
					slog.Info("==== LEADER ====", "term", raftService.raftNode.CurrentTerm, "leader_id", raftService.raftNode.Leader)
				}

				nextHeart = time.Now().Add(heart)
			}
			nextElect = time.Now().Add(election)

		}
	}
}

// stopGRPCServer stops the gRPC server.
func stopGRPCServer(raftService *RaftService) {
	// Stop the gRPC server
	raftService.server.server.GracefulStop()
	raftClientServer.server.GracefulStop()

	// Additional cleanup or shutdown logic here...

	slog.Info("=== Server stopped ===")
}

// startGRPCServer starts the gRPC server for the Raft node.
func startGRPCServer(raftService *RaftService, address string) (err error) {
	raftService.server.lis, err = net.Listen("tcp", address)
	if err != nil {
		err = fmt.Errorf("unable to set up listener. Reason: %w", err)
		return
	}

	raftService.server.server = grpc.NewServer()
	reflection.Register(raftService.server.server) // for debugging purposes

	pb.RegisterPeerConnServer(raftService.server.server, raftService)

	return
}
