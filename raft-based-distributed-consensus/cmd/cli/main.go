/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package main

import "raft-based-distributed-consensus/cmd/cli/cmd"

func main() {
	cmd.Execute()
}
