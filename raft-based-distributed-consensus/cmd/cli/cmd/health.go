/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"context"
	"log/slog"
	"os/signal"
	pb_client "raft-based-distributed-consensus/pb-client"
	"syscall"
	"time"

	"github.com/spf13/cobra"
)

// healthCmd represents the health command
var healthCmd = &cobra.Command{
	Use:   "health",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		slog.Info("health called")
		ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
		timeout := time.After(time.Duration(30) * time.Second)

		for {
			select {
			case <-ctx.Done():
				slog.Info("[ Graceful termination ]")
				return

			case <-timeout:
				slog.Error("Timeout!!")
				stop()

			default:
				client, err := NewRaftClient(Endpoint)
				if err != nil {
					slog.Error(err.Error())
					stop()
				}

				if res, err := client.client.SendCommand(ctx, &pb_client.SendCommandRequest{Command: "health"}); err != nil {
					slog.Error(err.Error())
				} else {
					slog.Info(res.String())
				}

				stop()
			}
		}

	},
}

func init() {
	rootCmd.AddCommand(healthCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// healthCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	healthCmd.Flags().StringVar(&Endpoint, "endpoint", "", "endpoint to reach for raft nodes")
	// healthCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
