/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"log/slog"
	"os/signal"
	pb_client "raft-based-distributed-consensus/pb-client"
	"syscall"
	"time"

	"github.com/spf13/cobra"
)

// membersCmd represents the members command
var membersCmd = &cobra.Command{
	Use:   "members",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("members called")

		ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
		timeout := time.After(time.Duration(30) * time.Second)
		for {
			select {
			case <-ctx.Done():
				slog.Info("[ Graceful termination ]")
				return

			case <-timeout:
				slog.Error("Timeout!!")
				stop()

			default:
				command := "members"

				client, err := NewRaftClient(Endpoint)
				if err != nil {
					slog.Error(err.Error())
					stop()
					continue
				}

				res, err := client.client.SendCommand(ctx, &pb_client.SendCommandRequest{Command: command})
				if err != nil {
					stop()
					continue
				}
				if res.Leader {
					var out []info
					raw := []byte(res.Response)
					if err := json.Unmarshal(raw, &out); err != nil {
						slog.Error(err.Error())
					}
					AmazingPrinter(out)
					stop()
					continue
				}

				slog.Warn(res.Response, "address", res.LeaderAddress)

				leader := res.LeaderAddress

				for i := 0; i < 5; i++ {
					client, err := NewRaftClient(leader)
					if err != nil {
						slog.Error(err.Error())
					}
					res, err := client.client.SendCommand(ctx, &pb_client.SendCommandRequest{Command: command})

					if err != nil {
						slog.Error(err.Error())
						continue
					}

					if res.Leader {
						//fmt.Println(res.Response)
						var out []info
						raw := []byte(res.Response)
						if err := json.Unmarshal(raw, &out); err != nil {
							slog.Error(err.Error())
						}
						AmazingPrinter(out)
						stop()
						return
					}

				}
				slog.Error("Failed to connect")
				stop()
			}
		}
	},
}

func AmazingPrinter(data []info) {
	for _, i := range data {
		fmt.Println("@@ Member @@")
		fmt.Printf("ID: %d\n", i.ID)
		fmt.Printf("Status: %s\n", i.Status)
		fmt.Printf("ClientRPC: %s\n", i.Client)
		fmt.Printf("MemberRPC: %s\n", i.Addr)
		fmt.Println("@@@@@@@@@@@@\n")
	}
}

func init() {
	rootCmd.AddCommand(membersCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// membersCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// membersCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	membersCmd.Flags().StringVar(&Endpoint, "endpoint", "", "endpoint to reach for raft nodes")
}
