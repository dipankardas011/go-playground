/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"context"
	"fmt"
	"log/slog"
	"os/signal"
	pb_client "raft-based-distributed-consensus/pb-client"
	"syscall"
	"time"

	"github.com/spf13/cobra"
)

// setCmd represents the set command
var setCmd = &cobra.Command{
	Use:   "set",
	Short: "sets data according to the key and value",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		slog.Info("set called")
		ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
		timeout := time.After(time.Duration(30) * time.Second)

		for {
			select {
			case <-ctx.Done():
				slog.Info("[ Graceful termination ]")
				return

			case <-timeout:
				slog.Error("Timeout!!")
				stop()

			default:
				command := fmt.Sprintf("set %s", data)

				client, err := NewRaftClient(Endpoint)
				if err != nil {
					slog.Error(err.Error())
					stop()
					continue
				}

				res, err := client.client.SendCommand(ctx, &pb_client.SendCommandRequest{Command: command})
				if err != nil {
					stop()
					continue
				}
				if res.Leader {
					slog.Info(res.Response)
					stop()
					continue
				}

				slog.Warn(res.Response, "address", res.LeaderAddress)

				leader := res.LeaderAddress

				for i := 0; i < 5; i++ {
					client, err := NewRaftClient(leader)
					if err != nil {
						slog.Error(err.Error())
					}
					res, err := client.client.SendCommand(ctx, &pb_client.SendCommandRequest{Command: command})

					if err != nil {
						slog.Error(err.Error())

					}
					if res.Leader {
						slog.Info(res.Response)
						stop()
						return
					}
				}
				slog.Error("Failed to connect")
				stop()
			}
		}

	},
}

func init() {
	rootCmd.AddCommand(setCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// setCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	setCmd.Flags().StringVar(&data, "data", "name=dipankar", "data to insert in the db")
	setCmd.Flags().StringVar(&Endpoint, "endpoint", "", "endpoint to reach for raft nodes")

	// setCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
