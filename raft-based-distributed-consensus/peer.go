package main

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"raft-based-distributed-consensus/pb"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// AppendEntries Invoked by leader to replicate log entries (§5.3); also used as heartbeat (§5.2).
func (raftService *RaftService) AppendEntries(ctx context.Context, req *pb.AppendEntriesReq) (*pb.AppendEntriesRes, error) {
	raftService.mx.Lock()
	defer raftService.mx.Unlock()
	slog.Info("AppendEntries is invoked", "request", req)

	res := &pb.AppendEntriesRes{
		Term: raftService.raftNode.CurrentTerm,
	}

	if req.Term < raftService.raftNode.CurrentTerm {
		return nil, errors.New("term < currentTerm")
	}

	raftService.raftNode.Leader = req.LeaderId
	raftService.raftNode.CurrentTerm = req.Term

	res.Success = true

	return res, nil
}

// RequestVote Invoked by candidates to gather votes (§5.2).
func (raftService *RaftService) RequestVote(ctx context.Context, req *pb.RequestVoteReq) (*pb.RequestVoteRes, error) {
	raftService.mx.Lock()
	defer raftService.mx.Unlock()

	slog.Info("RequestVote is invoked", "request", req)
	res := &pb.RequestVoteRes{
		Term: req.Term,
	}
	if req.Term <= raftService.raftNode.CurrentTerm {
		return nil, fmt.Errorf("candidate proposal rejected. Of Node: %v", req.CandidateId)
	}
	if raftService.raftNode.VotedFor == 0 || raftService.raftNode.VotedFor == req.CandidateId {
		// Grant vote
		raftService.raftNode.VotedFor = req.CandidateId
		raftService.raftNode.Type = TypeFollower
		// raftService.raftNode.Leader = req.CandidateId
		raftService.raftNode.CurrentTerm = req.Term
		res.VoteGranted = true
	} else {
		return nil, fmt.Errorf("Already voted")
	}

	return res, nil
}

// NewRaftClient creates a new Raft client.
func NewRaftClient(addr string) (*RaftClient, error) {

	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	}

	conn, err := grpc.Dial(addr, opts...)
	if err != nil {
		return nil, err
	}

	return &RaftClient{
		conn:   conn,
		client: pb.NewPeerConnClient(conn),
	}, nil
}
