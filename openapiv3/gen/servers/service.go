// Code generated by goa v3.12.4, DO NOT EDIT.
//
// servers service
//
// Command:
// $ goa gen openapi/design

package servers

import (
	"context"
)

// ksctl server handlers
type Service interface {
	// CreateCluster implements create cluster.
	CreateCluster(context.Context, *Metadata) (res *Response, err error)
	// GetClusters implements get clusters.
	GetClusters(context.Context) (res *Response, err error)
}

// ServiceName is the name of the service as defined in the design. This is the
// same value that is set in the endpoint request contexts under the ServiceKey
// key.
const ServiceName = "servers"

// MethodNames lists the service method names as defined in the design. These
// are the same values that are set in the endpoint request contexts under the
// MethodKey key.
var MethodNames = [2]string{"create cluster", "get clusters"}

// Metadata is the payload type of the servers service create cluster method.
type Metadata struct {
	Name   string
	Region string
	Cloud  string
	Distro string
	NoWP   *int32
	NoCP   *int32
	NoDS   *int32
	NoMP   *int32
}

// Response is the result type of the servers service create cluster method.
type Response struct {
	// was the operation successful
	OK *bool
	// reason in terms of error
	Error *string
	// response
	Response any
}
