// Code generated by goa v3.12.4, DO NOT EDIT.
//
// HTTP request path constructors for the servers service.
//
// Command:
// $ goa gen openapi/design

package client

// CreateClusterServersPath returns the URL path to the servers service create cluster HTTP endpoint.
func CreateClusterServersPath() string {
	return "/create"
}

// GetClustersServersPath returns the URL path to the servers service get clusters HTTP endpoint.
func GetClustersServersPath() string {
	return "/get"
}
