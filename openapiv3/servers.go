package ksctl

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"log"
	servers "openapi/gen/servers"
	"os"

	control_pkg "github.com/kubesimplify/ksctl/api/controllers"
	civo_pkg "github.com/kubesimplify/ksctl/api/provider/civo"
	"github.com/kubesimplify/ksctl/api/resources"
	"github.com/kubesimplify/ksctl/api/resources/controllers"
	"github.com/kubesimplify/ksctl/api/utils"
)

var (
	cli        *resources.CobraCmd
	controller controllers.Controller

	dir = fmt.Sprintf("%s/ksctl-dummy-%s", os.TempDir(), generateRandomString(5))
)

// servers service example implementation.
// The example methods log the requests and return zero values.
type serverssrvc struct {
	logger *log.Logger
}

// NewServers returns the servers service implementation.
func NewServers(logger *log.Logger) servers.Service {
	return &serverssrvc{logger}
}

func generateRandomString(length int) string {
	b := make([]byte, length)
	_, err := rand.Read(b)
	if err != nil {
		panic(err)
	}
	return base64.StdEncoding.EncodeToString(b)
}

func ExecuteManagedRun() error {

	var err error

	_, err = controller.CreateManagedCluster(&cli.Client)
	if err != nil {
		return err
	}
	//
	// _, err = controller.DeleteManagedCluster(&cli.Client)
	// if err != nil {
	// 	return err
	// }
	return nil
}

// CreateCluster implements create cluster.
func (s *serverssrvc) CreateCluster(ctx context.Context, p *servers.Metadata) (res *servers.Response, err error) {

	if err := os.Setenv(utils.KSCTL_FAKE_FLAG, "1"); err != nil {
		s.logger.Fatalf("Failed to set fake env %v", err)
	}
	cli = &resources.CobraCmd{}
	controller = control_pkg.GenKsctlController()

	cli.Client.Metadata.ClusterName = p.Name
	cli.Client.Metadata.StateLocation = utils.STORE_LOCAL
	cli.Client.Metadata.K8sDistro = p.Distro

	if _, err := control_pkg.InitializeStorageFactory(&cli.Client, true); err != nil {
		panic(err)
	}

	cli.Client.Metadata.Region = p.Region
	cli.Client.Metadata.Provider = p.Cloud
	cli.Client.Metadata.ManagedNodeType = "g4s.kube.small"
	cli.Client.Metadata.NoMP = int(*p.NoMP)

	_ = os.Setenv(utils.KSCTL_TEST_DIR_ENABLED, dir)
	azManaged := utils.GetPath(utils.CLUSTER_PATH, utils.CLOUD_CIVO, utils.CLUSTER_TYPE_MANG)
	azHA := utils.GetPath(utils.CLUSTER_PATH, utils.CLOUD_CIVO, utils.CLUSTER_TYPE_HA)

	if err := os.MkdirAll(azManaged, 0755); err != nil {
		panic(err)
	}

	if err := os.MkdirAll(azHA, 0755); err != nil {
		panic(err)
	}

	fmt.Println("Created tmp directories")

	// Return
	ok := false
	errStr := ""
	if err := ExecuteManagedRun(); err != nil {
		ok = false
		errStr = err.Error()
	} else {
		ok = true
		errStr = "Nothing"
	}

	type Response struct {
		Kubeconfig string
	}

	res = &servers.Response{OK: &ok, Error: &errStr, Response: Response{Kubeconfig: "/tmp/xyz/abcd/kubeconfig"}}
	s.logger.Print("servers.create cluster")

	return
}

// GetClusters implements get clusters.
func (s *serverssrvc) GetClusters(ctx context.Context) (res *servers.Response, err error) {
	b := true

	if err := os.Setenv(utils.KSCTL_FAKE_FLAG, "1"); err != nil {
		s.logger.Fatalf("Failed to set fake env %v", err)
	}
	cli = &resources.CobraCmd{}

	cli.Client.Metadata.StateLocation = utils.STORE_LOCAL

	if _, err := control_pkg.InitializeStorageFactory(&cli.Client, true); err != nil {
		panic(err)
	}
	data, err := civo_pkg.GetRAWClusterInfos(cli.Client.Storage)
	if err != nil {
		return
	}
	res = &servers.Response{
		OK:       &b,
		Response: data,
	}
	s.logger.Print("servers.get clusters")
	return
}
