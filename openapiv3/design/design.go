package design

import (
	. "goa.design/goa/v3/dsl" // https://pkg.go.dev/goa.design/goa/v3@v3.12.4/dsl#API
)

var _ = API("ksctl", func() {
	Title("ksctl")                         // Title used in documentation
	Description("kubernetes in one place") // Description used in documentation
	Version("2.0")                         // Version of API
	Contact(func() {                       // Contact info
		Name("Dipankar Das")
		Email("demo@gmail.com")
		URL("https://dipankar-das.com")
	})
	Docs(func() { // Documentation links
		Description("ksctl docs")
		URL("https://kubesimplify.github.io/ksctl/")
	})
	Server("addersvr", func() {
		Host("development", func() {
			URI("http://localhost:8080")
			// URI("grpc://localhost:8080")
		})
	})
})

var MetadataKsctl = Type("Metadata", func() {
	Description("Type to trigger create functionality")

	Attribute("name", String, func() {
		MinLength(1)
	})

	Attribute("region", String, func() {
		MinLength(1)
	})

	Attribute("cloud", String)

	Attribute("distro", String)

	Attribute("noWP", Int32, func() {
		Minimum(0)
		Maximum(10)
	})

	Attribute("noCP", Int32, func() {
		Minimum(3)
		Maximum(10)
	})

	Attribute("noDS", Int32, func() {
		Minimum(1)
		Maximum(1)
	})

	Attribute("noMP", Int32, func() {
		Minimum(1)
		Maximum(1)
	})

	Required("name", "region", "cloud", "distro") // Required attributes
})

var ResultKsctl = Type("Response", func() {

	Description("response type")

	Attribute("ok", Boolean, "was the operation successful")
	Attribute("error", String, "reason in terms of error")
	Attribute("response", Any, "response")
})

var _ = Service("servers", func() {
	Description("ksctl server handlers")

	Method("create cluster", func() {
		Payload(MetadataKsctl)
		Result(ResultKsctl)
		HTTP(func() {
			POST("/create")
		})
	})

	Method("get clusters", func() {
		Result(ResultKsctl)
		HTTP(func() {
			GET("/get")
		})
	})

	Files("/openapi3.json", "./gen/http/openapi3.json")
	Files("/swaggerui/{*path}", "./swaggerui")
})
