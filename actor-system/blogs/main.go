package main

import (
	"fmt"
	"net/rpc"
)

type Message struct {
	To   string
	From string
	Body string
}

type Actor struct {
	Name     string
	Messages []Message
}

func (a *Actor) SendMessage(m Message) {
	a.Messages = append(a.Messages, m)
}

func (a *Actor) ProcessMessages() {
	for _, m := range a.Messages {
		fmt.Printf("Actor %s received message: %s\n", a.Name, m.Body)
	}
	a.Messages = nil
}

type ActorManager struct {
	Actors map[string]*Actor
}

func NewActorManager() *ActorManager {
	return &ActorManager{Actors: make(map[string]*Actor)}
}

func (s *ActorManager) RegisterActor(name string) {
	s.Actors[name] = &Actor{Name: name}
}

func (s *ActorManager) SendMessage(m Message) {
	if actor, ok := s.Actors[m.To]; ok {
		actor.SendMessage(m)
	} else {
		// send message to remote actor
		client, err := rpc.Dial("tcp", m.To)
		if err != nil {
			fmt.Printf("Error connecting to remote actor %s: %s\n", m.To, err)
			return
		}
		defer client.Close()
		var reply string
		err = client.Call("RemoteActor.ReceiveMessage", m, &reply)
		if err != nil {
			fmt.Printf("Error sending message to remote actor %s: %s\n", m.To, err)
			return
		}
	}
}

func main() {
	// create local actor system
	actorManager := NewActorManager()

	// register local actors
	actorManager.RegisterActor("actor1")
	actorManager.RegisterActor("actor2")

	// send message to local actor
	actorManager.SendMessage(Message{
		To:   "actor1",
		From: "actor2",
		Body: "Hello from actor2 to actor1",
	})

	// send message to remote actor
	actorManager.SendMessage(Message{
		To:   "localhost:1234",
		From: "actor1",
		Body: "Hello from actor1 to remote actor on localhost:1234",
	})

	// process messages in all actors
	for _, actor := range actorManager.Actors {
		actor.ProcessMessages()
	}
}
