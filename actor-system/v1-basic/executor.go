package main

import (
	"log"
	"sync"
	"time"

	"github.com/fatih/color"
)

type Executor struct {
	_id          uint64
	t            Task
	mu           sync.Mutex // guards
	taskPresent  bool
	workAssigned chan bool
	closeSignal  chan struct{}
}

func (e *Executor) AddTask(t Task) {
	e.mu.Lock()
	defer e.mu.Unlock()
	log.Printf("Adding task: %#v to actor_id: %v\n", t, e._id)
	e.workAssigned = make(chan bool)
	e.closeSignal = make(chan struct{})
	e.t = t
	e.taskPresent = true
	go func() { e.workAssigned <- true }()
}

func (e *Executor) Start() {
	color.HiGreen("Booted the actor, id: %v", e._id)
}

func (e *Executor) Run() {
	color.HiGreen("Running task in actor, id: %v", e._id)
	go func() {
		ticker := time.NewTicker(2 * time.Second)
		for {
			select {
			case <-e.workAssigned:
				e.mu.Lock()
				err := e.t.Execute()
				log.Println("Error", err)
				e.taskPresent = false
				e.mu.Unlock()
			case <-e.closeSignal:
				color.Yellow("closing the actor, id: %v", e._id)
				return
			case <-ticker.C:
				if e.Available() {
					log.Printf("Actor_id: %v. Waiting for work or its processing", e._id)
				}
			}
		}
	}()
}

func (e *Executor) Stop() {
	e.mu.Lock()
	defer e.mu.Unlock()
	color.HiYellow("Stopped the actor, id: %v", e._id)
	close(e.closeSignal) // this will trigger the Run() to stop
}

func (e *Executor) Available() bool {
	e.mu.Lock()
	defer e.mu.Unlock()
	return !e.taskPresent
}
