package main

import (
	"log"
	"strconv"

	"github.com/fatih/color"
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			color.HiRed("PANIC occured: %+v", err)
		}
	}()
	var system ActorSystem = NewEventBus()
	defer func() {
		system.Shutdown()
	}()

	var work []*Work
	for i := 0; i < 4; i++ {
		work = append(work, &Work{
			in:  make(chan string),
			out: make(chan string),
		})
	}

	system.StartEventLoop()

	for _, w := range work {
		system.SubmitTask(w)
	}

	extra := &Work{
		in:  make(chan string),
		out: make(chan string),
	}
	system.SubmitTask(extra)

	color.HiYellow("Submited all the task. Now its time for running them")

	for i, w := range work {
		w.in <- "Hello from dipankar" + strconv.FormatInt(int64(i), 10)
	}

	extra.in <- "hello from === extra ==="

	work = append(work, extra)
	color.HiBlue("Fetch the results")
	for _, w := range work {
		ww := w
		go func() {
			o := <-ww.out
			log.Printf("Output: %s", o)
		}()
	}
}
