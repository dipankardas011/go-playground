package main

import (
	"sync"
	"time"

	"github.com/fatih/color"
)

type EventBus struct {
	actors []Actor
	wg     *sync.WaitGroup
	mu     sync.Mutex // guards
	lastID uint64
}

func NewEventBus() *EventBus {
	return &EventBus{
		wg: &sync.WaitGroup{},
	}
}

func (e *EventBus) CreateActor(t Task) {
	e.wg.Add(1)
	e.lastID++
	actor := &Executor{ // add a chan to get signal about completion
		_id: e.lastID,
	}
	actor.Start()
	actor.AddTask(t)
	e.actors = append(e.actors, actor)
}

func (e *EventBus) StartEventLoop() {
	color.HiGreen("EventBus triggering the actor to run the assigned tasks!")

	go func() {
		for {
			e.mu.Lock()
			actors := e.actors
			e.mu.Unlock()
			for _, actor := range actors {
				go func(a Actor) {
					if !a.Available() {
						a.Run()
						e.wg.Done()
					}
				}(actor)
			}
			time.Sleep(2 * time.Second)
		}
	}()
}

func (e *EventBus) SubmitTask(task Task) {
	e.mu.Lock()
	defer e.mu.Unlock()

	assigned := false
	for _, actor := range e.actors {
		if actor.Available() {
			e.wg.Add(1)
			actor.AddTask(task)
			assigned = true
			break
		}
	}

	if !assigned {
		e.CreateActor(task)
		color.HiCyan("EventBus created a new actor, and assigned task: %#v", task)
	} else {
		color.HiCyan("EventBus assigned task: %#v", task)
	}
}

func (e *EventBus) Shutdown() {
	color.HiYellow("EventBus shutdown triggered!")

	e.wg.Wait()
	for _, actor := range e.actors {
		// check if actor is not till free
		actor.Stop()
	}

	color.HiYellow("EventBus has shutdown!")
}
