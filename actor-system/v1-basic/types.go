package main

type Actor interface {
	AddTask(Task)
	Start()
	Run()
	Stop()
	Available() bool
}

type Task interface {
	Execute() error
}

type ActorSystem interface {
	StartEventLoop()
	SubmitTask(Task)
	Shutdown()
}
