package main

import (
	"fmt"
	"log"
	"math/rand"
	"time"

	"github.com/fatih/color"
)

type Work struct {
	in  chan string
	out chan string
}

func (w *Work) Execute() error {
	args := <-w.in
	color.HiMagenta("Here we go")
	log.Printf("Executing having input as: %s", args)
	time.Sleep(time.Duration(rand.Intn(5)) * time.Second)
	w.out <- fmt.Sprintf("completed the execution with: %s", args)
	return nil
}
