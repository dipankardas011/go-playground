package main

import (
	"context"
	"github.com/fatih/color"
	"log/slog"
	"math"
	"math/rand"
	"sync"
)

type ActorEngine struct {
	process Processor
	mu      sync.Mutex
}

func NewEngine() (*ActorEngine, error) {
	slog.Log(context.Background(), slog.LevelInfo, color.HiGreenString("Actor Engine"), "status", "started")
	a := &ActorEngine{
		process: &Process{
			actor: make(map[PID]*ProcessActor),
			wg:    make(map[PID]*sync.WaitGroup),
		},
	}

	go func() {
		a.process.Run()
	}()

	return a, nil
}

func (eng *ActorEngine) Attach(p Producer, actorName string) PID {
	pid := PID(rand.Intn(math.MaxInt))
	a := &Actor{
		ctx: &ActorContext{
			pid: pid,
		},
		name:    actorName,
		options: p,
	}

	a.Start()
	eng.process.Add(pid, a)

	return pid
}

func (eng *ActorEngine) Send(p PID, msg any) {
	eng.mu.Lock()
	defer eng.mu.Unlock()

	eng.process.SendSignal(p, msg)

}

func (eng *ActorEngine) Terminate(_ PID) {
	eng.process.Stop()
	slog.Log(context.Background(), slog.LevelInfo, color.HiYellowString("Actor Engine"), "status", "stopped")
}
