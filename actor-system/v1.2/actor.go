package main

import (
	"log/slog"
	"sync"

	"github.com/fatih/color"
)

type ActorContext struct {
	pid           PID
	resultMessage any
}

func (ctx *ActorContext) Message() any {
	return ctx.resultMessage
}

type Actor struct {
	mu      sync.Mutex
	ctx     *ActorContext
	name    string
	options Producer
}

func (a *Actor) SetResult(m any) {
	a.mu.Lock()
	defer a.mu.Unlock()

	a.ctx.resultMessage = m
}

func (a *Actor) Start() {
	a.SetResult(EventStarted)
	a.options().Receive(a.ctx)
}

func (a *Actor) Stopped() {
	a.SetResult(EventStopped)
	a.options().Receive(a.ctx)
}

func (a *Actor) Execute(msg any) error {
	a.mu.Lock()
	color.HiGreen("Running: %#v", msg)
	slog.Info("doing some task")
	a.mu.Unlock()

	a.SetResult(&ActorResponse{data: "[ACK] response from the actor as result"})
	a.options().Receive(a.ctx)
	return nil
}
