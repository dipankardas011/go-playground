package main

import (
	"github.com/fatih/color"
	"log/slog"
	"sync"
	"time"
)

type ProcessActor struct {
	A   *Actor
	msg chan any
}

type Process struct {
	mu    sync.RWMutex
	actor map[PID]*ProcessActor
	wg    map[PID]*sync.WaitGroup
}

func (proc *Process) SendSignal(p PID, msg any) {
	proc.mu.Lock()
	defer proc.mu.Unlock()

	if _, ok := proc.wg[p]; !ok {
		proc.wg[p] = &sync.WaitGroup{}
	}
	proc.wg[p].Add(1)
	proc.actor[p].msg <- msg
}

func (proc *Process) Add(p PID, a *Actor) {
	proc.mu.Lock()
	defer proc.mu.Unlock()
	if _, ok := proc.actor[p]; ok {
		slog.Warn("already attached")
		return
	}
	proc.actor[p] = &ProcessActor{}
	proc.actor[p].A = a
	proc.actor[p].msg = make(chan any, 10)
}

func (proc *Process) Run() {
	go func() {
		for {
			proc.mu.RLock()
			actor := proc.actor
			proc.mu.RUnlock()
			for pid, a := range actor {
				msg := <-a.msg
				if err := a.A.Execute(msg); err != nil {
					color.HiRed("Error: %v", err)
				}
				time.Sleep(500 * time.Millisecond)
				proc.mu.Lock()
				proc.wg[pid].Done()
				proc.mu.Unlock()
			}
		}
	}()
}

func (proc *Process) Stop() {
	for pid, a := range proc.actor {
		proc.wg[pid].Wait()
		a.A.Stopped()
	}
}
