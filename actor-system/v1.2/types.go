package main

type ActorEvent string

const (
	EventStarted ActorEvent = "started"
	EventStopped ActorEvent = "stopped"
)

type PID uint64

type Producer func() Receiver

type Receiver interface {
	Receive(*ActorContext)
}

type Processor interface {
	Run()
	Stop()
	Add(p PID, a *Actor)
	SendSignal(p PID, msg any)
}

type ActorResponse struct {
	data any
}
