package main

import (
	"fmt"
	"log/slog"
	"sync"

	"github.com/fatih/color"
)

type message struct {
	data any
}

type handler struct{}

func newHandler() Receiver {
	return &handler{}
}

func (m *handler) Receive(ctx *ActorContext) {

	switch msg := ctx.Message().(type) {
	case ActorEvent:

		switch msg {
		case EventStarted:
			slog.Info(color.HiCyanString("Actor started"))
		case EventStopped:
			slog.Info(color.HiCyanString("Actor stopped"))
		}

	case *ActorResponse:
		slog.Info(color.HiCyanString("Message recievied"), "data", msg.data)
	default:
		color.HiRed("DEFAULT CASE: %t, %#+v\n", msg, msg)
	}
}

func main() {

	engine, err := NewEngine()
	if err != nil {
		panic(err)
	}

	pid := engine.Attach(newHandler, "actor-1")

	wg := sync.WaitGroup{}
	for i := 0; i < 3; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			engine.Send(pid, &message{data: fmt.Sprintf("hello world, idx: %d", i)})
		}(i)
	}
	wg.Wait()

	engine.Terminate(pid)
}
