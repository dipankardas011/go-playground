package main

import (
	"context"
	"fmt"
	"math/rand"
	"os/signal"
	"syscall"
	"time"
)

// lets say we have multiple channels
func fanIn(inp1, inp2 <-chan string) <-chan string {
	c := make(chan string)
	go func() {
		for {
			select {
			case out1 := <-inp1:
				c <- out1
			case out2 := <-inp2:
				c <- out2
			}
		}
	}()
	return c
}

func talk(name string) <-chan string {
	c := make(chan string)
	// do something
	go func() {
		for {
			c <- fmt.Sprintf("%s {talking}\n", name)
			time.Sleep(time.Duration(rand.Intn(2)) * time.Second)
		}
	}()
	return c
}

func main() {
	c := fanIn(talk("dipankar"), talk("xyz"))
	timeout := time.After(time.Duration(30) * time.Second)
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)

	for {
		select {
		case x := <-c:
			fmt.Println(x)
		case <-time.After(time.Duration(3) * time.Second):
			fmt.Println("=== timeout of each case ===")
		case <-timeout:
			fmt.Println("=== entire proces timeout ===")
			stop()
		case <-ctx.Done():
			fmt.Println("[ Graceful termination ]")
			return
		}
	}
}
