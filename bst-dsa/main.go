package main

import "fmt"

type Node struct {
	Val   int
	Left  *Node
	Right *Node
}

func (tree *Node) Print() {
	var inOrder func(*Node)
	inOrder = func(node *Node) {
		if node != nil {
			inOrder(node.Left)
			fmt.Print(node.Val, " ")
			inOrder(node.Right)
		}
	}

	inOrder(tree)
	fmt.Printf("\n")
}

func (tree *Node) Mirror() {
	var mirror func(*Node) *Node
	mirror = func(n *Node) *Node {
		if n != nil {
			left := mirror(n.Left)
			right := mirror(n.Right)

			n.Left = right
			n.Right = left
			return n
		} else {
			return nil
		}
	}

	tree = mirror(tree)
}

func main() {
	/*
			6
		  4
		1  5

		6
			4
		  5   1
	*/
	var root *Node = &Node{Val: 6}
	root.Left = &Node{Val: 4}
	root.Left.Left = &Node{Val: 1}
	root.Left.Right = &Node{Val: 5}
	root.Print()
	root.Mirror()
	root.Print()
}
