package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	networkingv1 "k8s.io/api/networking/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	"k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/clientcmd"
)

func useK8sClient() {

	// Load kubeconfig
	kubeconfig := os.Getenv("HOME") + "/.kube/config"
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		panic(err)
	}

	// Create the dynamic client
	dynClient, err := dynamic.NewForConfig(config)
	if err != nil {
		panic(err)
	}
	apiextensionsClient, err := clientset.NewForConfig(config)
	if err != nil {
		panic(err)
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err)
	}

	// Get the manifest
	resp, err := http.Get("https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml")
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	if _, err := clientset.CoreV1().Namespaces().Create(context.Background(), &corev1.Namespace{ObjectMeta: v1.ObjectMeta{Name: "argocd"}}, v1.CreateOptions{}); err != nil {
		if apierrors.IsAlreadyExists(err) {
			_, err = clientset.CoreV1().Namespaces().Update(context.Background(), &corev1.Namespace{ObjectMeta: v1.ObjectMeta{Name: "argocd"}}, v1.UpdateOptions{})
			if err != nil {
				panic(err)
			}
		} else {
			panic(err)
		}
	}

	// Split the manifest into individual resources
	resources := strings.Split(string(body), "---")
	if err := apiextensionsv1.AddToScheme(scheme.Scheme); err != nil {
		panic(err)
	}
	for _, resource := range resources {
		// fmt.Println(resource)
		// Decode the resource into an unstructured object
		decUnstructured := scheme.Codecs.UniversalDeserializer().Decode
		obj, _, err := decUnstructured([]byte(resource), nil, nil)
		if err != nil {
			panic(err)
		}

		switch o := obj.(type) {
		case *unstructured.Unstructured:

			fmt.Printf("unstructured type %T\n", o)
			// Get the resource's metadata
			gvk := o.GroupVersionKind()
			gvr, _ := meta.UnsafeGuessKindToResource(gvk)

			fmt.Println(resource)

			// Create or update the resource
			_, err := dynClient.Resource(gvr).Namespace("argocd").Create(context.Background(), o, v1.CreateOptions{})
			if err != nil {
				if apierrors.IsAlreadyExists(err) {
					_, err = dynClient.Resource(gvr).Namespace("argocd").Update(context.Background(), o, v1.UpdateOptions{})
					if err != nil {
						panic(err)
					}
				} else {
					panic(err)
				}
			}
		case *apiextensionsv1.CustomResourceDefinition:
			_, err := apiextensionsClient.ApiextensionsV1().CustomResourceDefinitions().Create(context.Background(), o, v1.CreateOptions{})
			if err != nil {
				if apierrors.IsAlreadyExists(err) {
					_, err = apiextensionsClient.ApiextensionsV1().CustomResourceDefinitions().Update(context.Background(), o, v1.UpdateOptions{})
					if err != nil {
						panic(err)
					}
				} else {
					panic(err)
				}
			}
		case *appsv1.Deployment:
			fmt.Printf("Deployment %T\n", o)
			_, err := clientset.AppsV1().Deployments("argocd").Create(context.Background(), o, v1.CreateOptions{})
			if err != nil {
				if apierrors.IsAlreadyExists(err) {
					_, err = clientset.AppsV1().Deployments("argocd").Update(context.Background(), o, v1.UpdateOptions{})
					if err != nil {
						panic(err)
					}
				} else {
					panic(err)
				}
			}

		case *corev1.Service:
			fmt.Printf("service %T\n", o)
			_, err := clientset.CoreV1().Services("argocd").Create(context.Background(), o, v1.CreateOptions{})
			if err != nil {
				if apierrors.IsAlreadyExists(err) {
					_, err = clientset.CoreV1().Services("argocd").Update(context.Background(), o, v1.UpdateOptions{})
					if err != nil {
						panic(err)
					}
				} else {
					panic(err)
				}
			}

		case *corev1.ServiceAccount:
			fmt.Printf("serviceaccount %T\n", o)
			_, err := clientset.CoreV1().ServiceAccounts("argocd").Create(context.Background(), o, v1.CreateOptions{})
			if err != nil {
				if apierrors.IsAlreadyExists(err) {
					_, err = clientset.CoreV1().ServiceAccounts("argocd").Update(context.Background(), o, v1.UpdateOptions{})
					if err != nil {
						panic(err)
					}
				} else {
					panic(err)
				}
			}

		case *corev1.ConfigMap:
			fmt.Printf("configmap %T\n", o)
			_, err := clientset.CoreV1().ConfigMaps("argocd").Create(context.Background(), o, v1.CreateOptions{})
			if err != nil {
				if apierrors.IsAlreadyExists(err) {
					_, err = clientset.CoreV1().ConfigMaps("argocd").Update(context.Background(), o, v1.UpdateOptions{})
					if err != nil {
						panic(err)
					}
				} else {
					panic(err)
				}
			}

		case *corev1.Secret:
			fmt.Printf("Secret %T\n", o)
			_, err := clientset.CoreV1().Secrets("argocd").Create(context.Background(), o, v1.CreateOptions{})
			if err != nil {
				if apierrors.IsAlreadyExists(err) {
					_, err = clientset.CoreV1().Secrets("argocd").Update(context.Background(), o, v1.UpdateOptions{})
					if err != nil {
						panic(err)
					}
				} else {
					panic(err)
				}
			}

		case *appsv1.StatefulSet:
			fmt.Printf("Statefulset %T\n", o)
			_, err := clientset.AppsV1().StatefulSets("argocd").Create(context.Background(), o, v1.CreateOptions{})
			if err != nil {
				if apierrors.IsAlreadyExists(err) {
					_, err = clientset.AppsV1().StatefulSets("argocd").Update(context.Background(), o, v1.UpdateOptions{})
					if err != nil {
						panic(err)
					}
				} else {
					panic(err)
				}
			}

		case *rbacv1.ClusterRole:
			fmt.Printf("ClusterRole %T\n", o)
			_, err := clientset.RbacV1().ClusterRoles().Create(context.Background(), o, v1.CreateOptions{})
			if err != nil {
				if apierrors.IsAlreadyExists(err) {
					_, err = clientset.RbacV1().ClusterRoles().Update(context.Background(), o, v1.UpdateOptions{})
					if err != nil {
						panic(err)
					}
				} else {
					panic(err)
				}
			}

		case *rbacv1.ClusterRoleBinding:
			fmt.Printf("ClusterRoleBinding %T\n", o)
			_, err := clientset.RbacV1().ClusterRoleBindings().Create(context.Background(), o, v1.CreateOptions{})
			if err != nil {
				if apierrors.IsAlreadyExists(err) {
					_, err = clientset.RbacV1().ClusterRoleBindings().Update(context.Background(), o, v1.UpdateOptions{})
					if err != nil {
						panic(err)
					}
				} else {
					panic(err)
				}
			}

		case *rbacv1.Role:
			fmt.Printf("Role %T\n", o)
			_, err := clientset.RbacV1().Roles("argocd").Create(context.Background(), o, v1.CreateOptions{})
			if err != nil {
				if apierrors.IsAlreadyExists(err) {
					_, err = clientset.RbacV1().Roles("argocd").Update(context.Background(), o, v1.UpdateOptions{})
					if err != nil {
						panic(err)
					}
				} else {
					panic(err)
				}
			}

		case *rbacv1.RoleBinding:
			fmt.Printf("RoleBinding %T\n", o)
			_, err := clientset.RbacV1().RoleBindings("argocd").Create(context.Background(), o, v1.CreateOptions{})
			if err != nil {
				if apierrors.IsAlreadyExists(err) {
					_, err = clientset.RbacV1().RoleBindings("argocd").Update(context.Background(), o, v1.UpdateOptions{})
					if err != nil {
						panic(err)
					}
				} else {
					panic(err)
				}
			}

		case *networkingv1.NetworkPolicy:
			fmt.Printf("NetworkPolicy %T\n", o)
			_, err := clientset.NetworkingV1().NetworkPolicies("argocd").Create(context.Background(), o, v1.CreateOptions{})
			if err != nil {
				if apierrors.IsAlreadyExists(err) {
					_, err = clientset.NetworkingV1().NetworkPolicies("argocd").Update(context.Background(), o, v1.UpdateOptions{})
					if err != nil {
						panic(err)
					}
				} else {
					panic(err)
				}
			}

		default:
			fmt.Printf("unexpected type %T\n", o)
		}
	}

	fmt.Printf(`
		kubectl port-forward svc/argocd-server -n argocd 8080:443
		kubectl get secret -n argocd argocd-initial-admin-secret -o json | jq -r '.data.password' | base64 -d
		and login to http://localhost:8080 with user admin and password from above
	`)

}

func main() {
	useK8sClient()
}
