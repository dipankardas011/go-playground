/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"

	controllersv1 "test-kubebuilder/api/v1"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
)

// ScaleReconciler reconciles a Scale object
type ScaleReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=controllers.ksctl.com,resources=scales,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=controllers.ksctl.com,resources=scales/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=controllers.ksctl.com,resources=scales/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Scale object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.4/pkg/reconcile
func (r *ScaleReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := log.FromContext(ctx)

	instance := &controllersv1.Scale{}

	err := r.Get(ctx, req.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {

			// TODO: use deployment instead of a POD for use of update flag
			fmt.Println("deleted instance", "instanceName", req.Name)
			fmt.Println("deleted instance", "instanceNameSpace", req.Namespace)

			if err := r.Client.Delete(ctx, &corev1.Pod{
				TypeMeta: v1.TypeMeta{
					Kind:       "Pod",
					APIVersion: "v1",
				},
				ObjectMeta: v1.ObjectMeta{
					Name:      req.Name + "-scale",
					Namespace: req.Namespace,
				},
			}, &client.DeleteOptions{
				GracePeriodSeconds: func() *int64 {
					a := int64(0)
					return &a
				}(),
			}); err != nil {
				return ctrl.Result{}, err
			}

			return ctrl.Result{}, nil
		}
		log.Error(err, "Failed to fetch <kind>")
		return ctrl.Result{}, nil
	}

	log.Info("created instance", "instance", instance, "instanceSpec", fmt.Sprintf("foo: `%s`; wehook: `%s`", instance.Spec.Foo, instance.Spec.WebhookField))
	if err := r.Client.Create(ctx, PodSpec(instance.Name+"-scale", instance.Namespace)); err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func PodSpec(name, ns string) *corev1.Pod {

	return &corev1.Pod{
		TypeMeta: v1.TypeMeta{
			Kind:       "Pod",
			APIVersion: "v1",
		},
		ObjectMeta: v1.ObjectMeta{
			Name:      name,
			Namespace: ns,
		},
		Spec: corev1.PodSpec{

			RestartPolicy: corev1.RestartPolicyNever,
			Containers: []corev1.Container{
				{
					Name:  "demo",
					Image: "nginx:alpine",
				},
			},
		},
	}
}

// SetupWithManager sets up the controller with the Manager.
func (r *ScaleReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&controllersv1.Scale{}).
		Complete(r)
}
