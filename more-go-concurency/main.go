package main

import (
	"context"
	"os/signal"
	"syscall"
	"time"

	"log/slog"
)

func main() {

	ctx, _ := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)

	onlyRunSingle := make(chan bool)
	go func() { onlyRunSingle <- true }()
	heart := time.Duration(5 * time.Second)
	election := time.Duration(6 * time.Second)

	var nextHeart, nextElect time.Time

	for {
		var fetchDelayHeart, fetchDelayElect time.Duration
		if now := time.Now(); nextHeart.After(now) {
			fetchDelayHeart = nextHeart.Sub(now)
		}
		heartInterv := time.After(fetchDelayHeart)

		if now := time.Now(); nextElect.After(now) {
			fetchDelayElect = nextElect.Sub(now)
		}
		electionInterv := time.After(fetchDelayElect)

		select {
		case <-ctx.Done():
			// Wait for termination signal
			slog.Info("Received termination signal. Shutting down...")

			return

		case <-heartInterv:
			slog.Info("Working heartbeat")
			nextHeart = time.Now().Add(heart)

		case <-electionInterv:
			slog.Info("election timeout")
			nextElect = time.Now().Add(election)
		}
	}
}
