```bash
# run the server
go run main.go
```

```bash
# for UI
grpcui --plaintext 127.0.0.1:8080
```

```bash
# for the grpcurl
grpcurl -plaintext -d '{"name": "cdcd", "region": "LON1", "ha": true}' 127.0.0.1:8080 custom.Logging.Create
```
