
const grpc = require('grpc');

const protoLoader = require('@grpc/proto-loader');

const packageDefinition = protoLoader.loadSync('proto/custom.proto', {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
});

const protoDescriptor = grpc.loadPackageDefinition(packageDefinition);

const client = new protoDescriptor.custom.Logging('localhost:8080', grpc.credentials.createInsecure());


// Call the streaming RPC
const call = client.Create({name: 'demo', region: 'LON1', ha: true});

// Handle streaming responses
call.on('data', (response) => {
    console.log(`Received: ${response.message}`);
});

call.on('end', () => {
    console.log('Server streaming ended.');
});

call.on('error', (error) => {
    console.error('Error:', error);
});
