package logger

import (
	"fmt"
	"io"
	"log/slog"
	"math"

	box "github.com/Delta456/box-cli-maker/v2"
	"github.com/fatih/color"
)

type Logger struct {
	logger     *slog.Logger
	moduleName string
}

func (l *Logger) SetPackageName(m string) {
	l.moduleName = m
}

func newLogger(out io.Writer, ver slog.Level, debug bool) *slog.Logger {
	if debug {
		return slog.New(slog.NewJSONHandler(out, &slog.HandlerOptions{
			Level: ver,
		}))
	}
	return slog.New(slog.NewTextHandler(out, &slog.HandlerOptions{
		Level: ver,
	}))
}

func NewDefaultLogger(verbose int, out io.Writer) *Logger {
	// LevelDebug Level = -4
	// LevelInfo  Level = 0
	// LevelWarn  Level = 4
	// LevelError Level = 8

	var ve slog.Level

	if verbose < 0 {
		ve = slog.LevelDebug

		return &Logger{logger: newLogger(out, ve, true)}

	} else if verbose < 4 {
		ve = slog.LevelInfo
	} else if verbose < 8 {
		ve = slog.LevelWarn
	} else {
		ve = slog.LevelError
	}

	return &Logger{logger: newLogger(out, ve, false)}
}

func (l *Logger) Print(msg string, args ...any) {
	args = append([]any{"package", l.moduleName}, args...)
	l.logger.Info(msg, args...)
}

func (l *Logger) Success(msg string, args ...any) {
	color.Set(color.FgGreen, color.Bold)
	defer color.Unset()
	args = append([]any{"package", l.moduleName}, args...)
	l.logger.Info(msg, args...)
}

func (l *Logger) Note(msg string, args ...any) {
	color.Set(color.FgBlue, color.Bold)
	defer color.Unset()
	args = append([]any{"package", l.moduleName}, args...)
	l.logger.Info(msg, args...)
}

func (l *Logger) Debug(msg string, args ...any) {
	defer color.Unset()
	args = append([]any{"package", l.moduleName}, args...)
	l.logger.Debug(msg, args...)
}

func (l *Logger) Error(msg string, args ...any) {
	color.Set(color.FgHiRed, color.Bold)
	defer color.Unset()
	args = append([]any{"package", l.moduleName}, args...)
	l.logger.Error(msg, args...)
}

func (l *Logger) NewError(format string, args ...any) error {
	l.Debug(format, args...)
	args = append([]any{"package", l.moduleName}, args...)
	return fmt.Errorf(format, args...)
}

func (l *Logger) Warn(msg string, args ...any) {
	color.Set(color.FgYellow, color.Bold)
	defer color.Unset()
	args = append([]any{"package", l.moduleName}, args...)
	l.logger.Warn(msg, args...)
}

func (l *Logger) Box(title string, lines string) {
	px := 4
	if len(title) >= 2*px+len(lines) {
		// some maths
		px = int(math.Ceil(float64(len(title)-len(lines))/2)) + 1
	}
	l.Debug("PostUpdate Box", "px", px, "title", len(title), "lines", len(lines))
	Box := box.New(box.Config{Px: px, Py: 2, Type: "Bold", TitlePos: "Top", Color: "Cyan"})
	Box.Println(title, lines)
}
