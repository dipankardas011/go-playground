package main

import (
	// "crypto/tls"

	"context"
	"io"
	"ksctl-logger/pb"
	"log"
	"log/slog"
	"time"

	"google.golang.org/grpc"

	// "google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	// creds := credentials.NewTLS(&tls.Config{InsecureSkipVerify: false})

	opts := []grpc.DialOption{
		// grpc.WithTransportCredentials(creds),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	}

	url := "127.0.0.1:8080"

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	conn, err := grpc.DialContext(ctx, url, opts...)
	if err != nil {
		slog.Error("fail to dial", "reason", err)
	}
	defer conn.Close()

	client := pb.NewLoggingClient(conn)

	stream, err := client.Create(context.Background(), &pb.Request{Name: "demo", Region: "LON1", Ha: true})

	if err != nil {
		log.Fatalf("error while calling Create: %v", err)
	}
	// Receive messages from the server stream
	for {
		reply, err := stream.Recv()

		if err == io.EOF {
			// The stream has ended, exit the loop
			break
		}
		if err != nil {
			log.Fatalf("error while receiving response: %v", err)
		}
		log.Printf("Received: %s", reply.Message)
	}
}
