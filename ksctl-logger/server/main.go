package main

import (
	"fmt"
	"ksctl-logger/pb"
	"net"
	"os"

	"log/slog"

	control_pkg "github.com/kubesimplify/ksctl/pkg/controllers"
	"github.com/kubesimplify/ksctl/pkg/resources"
	"github.com/kubesimplify/ksctl/pkg/utils"
	"github.com/kubesimplify/ksctl/pkg/utils/consts"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
)

var dir = fmt.Sprintf("%s/ksctl-grpc-test", os.TempDir())

type CustomWriter struct {
	stream pb.Logging_CreateServer
}

func (e CustomWriter) Write(p []byte) (int, error) {
	e.stream.Send(&pb.Response{Message: string(p)})
	return len(p), nil
}

type srv struct {
	pb.UnimplementedLoggingServer
}

func (s *srv) Create(req *pb.Request, stream pb.Logging_CreateServer) error {

	cli := new(resources.KsctlClient)
	controller := control_pkg.GenKsctlController()

	cli.Metadata.ClusterName = req.Name
	cli.Metadata.StateLocation = consts.StoreLocal
	cli.Metadata.K8sDistro = consts.K8sK3s
	cli.Metadata.LogVerbosity = 0
	cli.Metadata.LogWritter = &CustomWriter{stream: stream}
	cli.Metadata.Region = req.Region
	cli.Metadata.Provider = consts.CloudCivo
	cli.Metadata.ManagedNodeType = "g4s.kube.small"
	cli.Metadata.NoMP = 2
	// if req.Ha {
	cli.Metadata.K8sVersion = "1.27.4"
	// }
	cli.Metadata.IsHA = req.Ha

	cli.Metadata.LoadBalancerNodeType = "fake.small"
	cli.Metadata.ControlPlaneNodeType = "fake.small"
	cli.Metadata.WorkerPlaneNodeType = "fake.small"
	cli.Metadata.DataStoreNodeType = "fake.small"
	cli.Metadata.NoCP = 5
	cli.Metadata.NoWP = 1
	cli.Metadata.NoDS = 3

	_ = os.Setenv(string(consts.KsctlCustomDirEnabled), dir)

	if err := os.MkdirAll(utils.GetPath(consts.UtilClusterPath, consts.CloudCivo, consts.ClusterTypeMang), 0755); err != nil {
		return status.Error(codes.Aborted, err.Error())
	}
	if err := os.MkdirAll(utils.GetPath(consts.UtilClusterPath, consts.CloudCivo, consts.ClusterTypeHa), 0755); err != nil {
		return status.Error(codes.Aborted, err.Error())
	}

	if err := control_pkg.InitializeStorageFactory(cli); err != nil {
		return status.Error(codes.Aborted, err.Error())
	}

	if req.Ha {
		if err := controller.CreateHACluster(cli); err != nil {
			return status.Error(codes.Aborted, err.Error())
		}
	} else {
		if err := controller.CreateManagedCluster(cli); err != nil {
			return status.Error(codes.Aborted, err.Error())
		}
	}

	return nil
}

func main() {
	if err := os.Setenv(string(consts.KsctlFakeFlag), "1"); err != nil {
		slog.Error("Failed to set fake env %v", err)
	}
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		slog.Error("unable to do http listener", "err", err)
	}

	s := grpc.NewServer()
	reflection.Register(s) // for debugging purposes

	pb.RegisterLoggingServer(s, &srv{}) // Register the server with the gRPC server

	slog.Info("Server started", "port", "8080")

	if err := s.Serve(listener); err != nil {
		slog.Error("failed to serve", "err", err)
	}
}
