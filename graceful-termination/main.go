package main

import (
	"context"
	"errors"
	"fmt"
	storage "graceful-termination/Storage"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/kubesimplify/ksctl/pkg/utils/consts"
)

func (i *Infrastructure) Start() error {
	fmt.Println("Started Infra")
	return nil
}

func (i *Infrastructure) Stop() error {
	fmt.Println("Stopped Infra")
	return nil
}

func (client *KsctlClient) CreateCluster(ctx context.Context) error {
	// TODO: next is to figure out how to make the configurationStore Write and Read based on channel signal
	client.operationsWG.Add(1)
	defer client.operationsWG.Done()
	log.Println("Create cluster triggered")

	var err error
	client.store, err = storage.GetClient()
	if err != nil {
		return err
	}

	if err := client.store.Setup(
		consts.KsctlCloud(client.Details.Cloud),
		client.Details.Region,
		client.Details.Name,
		consts.KsctlClusterType(client.Details.ClusterType),
	); err != nil {
		return err
	}

	if err := client.cloud.Start(); err != nil {
		return err
	}

	if client.store.AlreadyCreated(string(client.Details.Cloud), client.Details.Region, client.Details.Name, string(client.Details.ClusterType)) {
		return errors.New("Already created cluster")
	}
	time.Sleep(2 * time.Second) // give chance to user to terminate

	err = client.store.Write(nil)
	if err != nil {
		return err
	}

	if err := client.cloud.Stop(); err != nil {

		return err
	}
	return nil
}

func (client *KsctlClient) DeleteCluster(ctx context.Context) error {
	client.operationsWG.Add(1)
	defer client.operationsWG.Done()
	log.Println("Delete cluster triggered")

	var err error
	client.store, err = storage.GetClient()
	if err != nil {
		return err
	}

	if err := client.store.Setup(
		consts.KsctlCloud(client.Details.Cloud),
		client.Details.Region,
		client.Details.Name,
		consts.KsctlClusterType(client.Details.ClusterType),
	); err != nil {
		return err
	}

	if err := client.cloud.Start(); err != nil {
		return err
	}

	if !client.store.AlreadyCreated(string(client.Details.Cloud), client.Details.Region, client.Details.Name, string(client.Details.ClusterType)) {
		return errors.New("cluster absent from records")
	}

	_, err = client.store.Read()
	if err != nil {
		return err
	}

	err = client.store.DeleteCluster()
	if err != nil {
		return err
	}

	if err := client.cloud.Stop(); err != nil {
		return err
	}
	return nil
}

func (client *KsctlClient) Cleanup() error {
	log.Println("stopped trigger")
	if client.store != nil {
		if err := client.store.Cleanup(os.Getenv("OPERATION")); err != nil {
			return err
		}
	}
	client.operationsWG.Wait()
	log.Println("Stopped the cli")
	return nil
}

func ExampleSentChannel(no int, c chan<- string) {
	c <- fmt.Sprintf("Example: %d\n", no)
}

func cli() error {
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)

	c := make(chan string)
	for i := 0; i < 5; i++ {
		go ExampleSentChannel(i, c)
	}
	timeout := time.After(1 * time.Nanosecond)
	for i := 0; i < 5; i++ {
		select {
		case r := <-c:
			fmt.Println(r)
		case <-timeout:
			fmt.Println("timeout")
			close(c)
			return nil
		}
	}
	close(c)

	client := &KsctlClient{
		cloud:      &Infrastructure{},
		kubernetes: &Distribution{},
		Details:    Metadata{Cloud: CloudAWS, Name: "fake", ClusterType: TypeManaged, Region: "ap-south-1"},
	}

	var switchError error = func() error {
		var err error
		switch os.Getenv("OPERATION") {
		case "create":
			err = client.CreateCluster(ctx)
		case "delete":
			err = client.DeleteCluster(ctx)
		default:
			err = errors.New("invalid operation")
		}
		return err
	}()

	var resError error

	for {
		select {
		case <-ctx.Done():
			log.Println("context ended")
			if err := client.Cleanup(); err != nil {
				return fmt.Errorf("Cleanup err: %w", err)
			}
			return resError
		default:
			if switchError != nil {
				resError = switchError
			}
			stop()
		}
	}
}

func main() {

	defer func() {
		if err := recover(); err != nil {
			log.Printf("recovered from panic: %#+v\n", err)
		}
	}()

	if err := cli(); err != nil {
		panic(err)
	}

}
