package main

import (
	"sync"

	"github.com/kubesimplify/ksctl/pkg/utils/consts"
)

type ClusterType byte

type CloudProvider string

const (
	TypeHA      ClusterType = 0
	TypeManaged ClusterType = 1

	CloudAzure CloudProvider = "azure"
	CloudAWS   CloudProvider = "aws"
	CloudCivo  CloudProvider = "civo"
)

type Infrastructure struct{}

type Distribution struct{}

type ConfigurationStorage interface {
	Cleanup(operation string) error

	Setup(cloud consts.KsctlCloud, region, clusterName string, clusterType consts.KsctlClusterType) error

	Write(data any) error

	Read() (any, error)

	DeleteCluster() error

	AlreadyCreated(cloud, region, clustername, clusterType string) bool

	GetOneOrMoreClusters(cloud string, filters map[string]string) ([]any, error)
}

type Metadata struct {
	Name        string
	Region      string
	ClusterType ClusterType
	Cloud       CloudProvider
}

type KsctlClient struct {
	cloud *Infrastructure

	kubernetes *Distribution

	store ConfigurationStorage

	Details Metadata

	operationsWG sync.WaitGroup
}
