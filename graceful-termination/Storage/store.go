package storage

import (
	"log"

	"github.com/kubesimplify/ksctl/pkg/utils/consts"
)

type MongoDB struct {
	mongodbAgent any
	cloud        consts.KsctlCloud
	region       string
	clusterName  string
	clusterType  consts.KsctlClusterType
}

func (store *MongoDB) Cleanup(operation string) error {
	if operation == "create" {
		if err := store.Write(nil); err != nil {
			return err
		}
	}
	log.Println("Disconnected to $env:MONGODB_URL")
	return nil
}

func (store *MongoDB) connect() error {
	log.Println("Connected to $env:MONGODB_URL")
	return nil
}

func GetClient() (*MongoDB, error) {
	mongo := &MongoDB{}
	if err := mongo.connect(); err != nil {
		return nil, err
	}
	return mongo, nil
}

func (store *MongoDB) Setup(cloud consts.KsctlCloud, region, clusterName string, clusterType consts.KsctlClusterType) error {
	store.cloud = cloud
	store.region = region
	store.clusterName = clusterName
	store.clusterType = clusterType
	return nil
}

func (store *MongoDB) Write(data any) error {
	log.Println("WRITE invoked!")
	return nil
}

func (store *MongoDB) Read() (any, error) {
	log.Println("READ invoked!")
	return nil, nil
}

func (store *MongoDB) DeleteCluster() error {
	log.Println("DELETED CLUSTER invoked!")
	return nil
}

func (store *MongoDB) AlreadyCreated(cloud, region, clustername, clusterType string) bool {
	return false
}

func (store *MongoDB) GetOneOrMoreClusters(cloud string, filters map[string]string) ([]any, error) {
	return nil, nil
}
