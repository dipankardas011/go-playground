package main

import (
	"context"
	"errors"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/fatih/color"
)

type KsctlClient struct {
	Other any
	wg    *sync.WaitGroup
}

type Cloud struct {
	wg *sync.WaitGroup
}

func (c *Cloud) cloudDoing(ctx context.Context) error {

	c.wg.Add(1)
	color.HiBlue("Press stop")
	time.Sleep(2 * time.Second)

	chanD := make(chan struct{})
	go func() {
		defer c.wg.Done()
		defer close(chanD)
		color.HiGreen("Started first go routine")
		time.Sleep(2 * time.Second)
		color.HiGreen("Saving data first go routine")
	}()
	<-chanD
	if ctx.Err() != nil {
		// ctx is already dead
		return ctx.Err()
	}

	color.HiBlue("Press stop")
	time.Sleep(2 * time.Second)

	c.wg.Add(1)
	chanE := make(chan struct{})

	go func() {
		defer c.wg.Done()
		defer close(chanE)
		color.HiRed("Started second go routine")
		time.Sleep(2 * time.Second)
		color.HiRed("Saving data second go routine")
	}()
	<-chanE
	if ctx.Err() != nil {
		// ctx is already dead
		return ctx.Err()
	}
	color.Green("Done with cloud")
	return nil
}

func ControllerCreateCluster(ctx context.Context, client *KsctlClient) {
	time.Sleep(2 * time.Second)
	log.Println("Triggering the infra")
	c := &Cloud{wg: client.wg}
	if err := c.cloudDoing(ctx); err != nil {
		color.HiRed(err.Error())
	}
}
func ControllerDeleteCluster(ctx context.Context, client *KsctlClient) {}

func ControllerCleanup(client *KsctlClient) {
	log.Println("triggered cleanup")
	client.wg.Wait()

	log.Println("Done cleanup")
}

func main() {

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	ksctlClient := KsctlClient{wg: &sync.WaitGroup{}}
	// go func() {
	// 	for {
	// 		fmt.Printf("%#v\n", ksctlClient.wg)
	// 		time.Sleep(time.Second)
	// 	}
	// }()
	var switchError error = func() error {
		var err error
		switch os.Getenv("OPERATION") {
		case "create":
			ControllerCreateCluster(ctx, &ksctlClient)
		case "delete":
			ControllerDeleteCluster(ctx, &ksctlClient)
		default:
			err = errors.New("invalid operation")
		}
		return err
	}()

	for {
		select {
		case <-ctx.Done():
			log.Println("context ended")
			ControllerCleanup(&ksctlClient)
			return
		default:
			err := switchError
			log.Println("err. Reason:", err)
			stop()
		}
	}
}
