// Copyright 2015 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build ignore
// +build ignore

package main

import (
	"errors"
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/websocket"
)

var addr = flag.String("addr", "localhost:8080", "http service address")

var upgrader = websocket.Upgrader{} // use default options

type Demo struct {
	Msg string
	Err string
}

type Client struct {
	Name    *string
	Distro  *string
	Logger  *string
	Storage *string
	NoCP    *int
	NoWP    *int
	VmSize  *string
}

func (e *Client) String() string {
	return fmt.Sprintf("Name: %s\ndistro: %s\nlogger: %s\nstorage: %s\nnocp: %d\nnowp: %d\nvmsize: %s\n", *e.Name, *e.Distro, *e.Logger, *e.Storage, *e.NoCP, *e.NoWP, *e.VmSize)
}

func fieldValidation(c *websocket.Conn, req *Client) error {
	if req.Name == nil {
		return errors.New("Missing Name field")
	}

	if req.Distro == nil {
		return errors.New("Missing Distro field")
	}

	if req.Storage == nil {
		return errors.New("Missing Storage field")
	}

	if req.Logger == nil {
		return errors.New("Missing Logger field")
	}

	if req.Storage == nil {
		return errors.New("Missing Storage field")
	}

	if req.NoCP == nil {
		return errors.New("Missing NoCP field")
	}

	if req.NoWP == nil {
		return errors.New("Missing NoWP field")
	}

	if req.VmSize == nil {
		return errors.New("Missing VMSize field")
	}
	return nil
}

func echo(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()

	// stopping duplex communication
	// client can only recive its response

	// for {
	req := &Client{}

	err = c.ReadJSON(req)
	if err != nil {
		log.Println("read:", err)
		c.WriteJSON(Demo{Err: err.Error()})
		return
	}
	log.Printf("recv: %#v", req)

	if err := fieldValidation(c, req); err != nil {
		log.Println("Issue with req")
		c.WriteJSON(Demo{Err: err.Error()})
		return
	}

	log.Printf("recv: %+v", req)

	count := 5
	for i := 0; i < count; i++ {
		err = c.WriteJSON(Demo{Msg: *req.Name + " (Res) " + strconv.Itoa(i)})
		if err != nil {
			log.Println("write:", err)
			return
		}
		time.Sleep(2 * time.Second)
	}

	// }
}

func home(w http.ResponseWriter, r *http.Request) {
	homeTemplate.Execute(w, "ws://"+r.Host+"/echo")
}

func main() {
	flag.Parse()
	log.SetFlags(0)
	http.HandleFunc("/echo", echo)
	http.HandleFunc("/", home)
	log.Fatal(http.ListenAndServe(*addr, nil))
}

var homeTemplate = template.Must(template.New("").Parse(`
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<script>  
window.addEventListener("load", function(evt) {

    var output = document.getElementById("output");
    var input = document.getElementById("input");
    var ws;

    var print = function(message) {
        var d = document.createElement("div");
        d.textContent = message;
        output.appendChild(d);
        output.scroll(0, output.scrollHeight);
    };

    document.getElementById("open").onclick = function(evt) {
        if (ws) {
            return false;
        }
        ws = new WebSocket("{{.}}");
        ws.onopen = function(evt) {
            print("OPEN");
        }
        ws.onclose = function(evt) {
            print("CLOSE");
            ws = null;
        }
        ws.onmessage = function(evt) {
            print("RESPONSE: " + evt.data);
        }
        ws.onerror = function(evt) {
            print("ERROR: " + evt.data);
        }
        return false;
    };

    document.getElementById("send").onclick = function(evt) {
        if (!ws) {
            return false;
        }
		var str = json.stringify(input.value);
        print("SEND: " + str);
        ws.send(str);
        return false;
    };

    document.getElementById("close").onclick = function(evt) {
        if (!ws) {
            return false;
        }
        ws.close();
        return false;
    };

});
</script>
</head>
<body>
<table>
<tr><td valign="top" width="50%">
<p>Click "Open" to create a connection to the server, 
"Send" to send a message to the server and "Close" to close the connection. 
You can change the message and send multiple times.
<p>
<form>
<button id="open">Open</button>
<button id="close">Close</button>
<p><input id="input" type="text" value="Hello world!">
<button id="send">Send</button>
</form>
</td><td valign="top" width="50%">
<div id="output" style="max-height: 70vh;overflow-y: scroll;"></div>
</td></tr></table>
</body>
</html>
`))
