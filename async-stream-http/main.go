package main

import (
	"fmt"
	"net/http"
	"time"
)

func BasicStream(w http.ResponseWriter, r *http.Request) {
	// Set the headers to indicate that this response will be streamed
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(http.StatusOK)

	// Get the http.Flusher interface from the response writer
	flusher, ok := w.(http.Flusher)
	if !ok {
		http.Error(w, "Streaming not supported!", http.StatusInternalServerError)
		return
	}

	// Stream data to the client
	for i := 1; i <= 10; i++ {
		fmt.Fprintf(w, "Streaming message %d\n", i)

		// Flush the response to the client to ensure it gets sent immediately
		flusher.Flush()

		// Introduce a delay to simulate streaming behavior
		time.Sleep(1 * time.Second)
	}
}

func main() {
	http.HandleFunc("/stream", BasicStream)

	// Start the HTTP server
	http.ListenAndServe(":8080", nil)
}
