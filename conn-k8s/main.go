package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"k8s.io/client-go/tools/clientcmd"
)

const (
	LocationCaCert     = "/tmp/ca.crt"
	LocationClientCert = "/tmp/client.crt"
	LocationClientKey  = "/tmp/client.pem"
)

func Write(fileName string, data []byte) error {
	return os.WriteFile(fileName, data, 0770)
}

func HttpClient() (*http.Client, error) {

	caCert, err := os.ReadFile(LocationCaCert)
	if err != nil {
		fmt.Println("Error reading CA certificate:", err)
		return nil, err
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	cert, err := tls.LoadX509KeyPair(LocationClientCert, LocationClientKey)
	if err != nil {
		fmt.Println("Error loading client certificate and key:", err)
		return nil, err
	}

	tlsConfig := &tls.Config{
		RootCAs:      caCertPool,
		Certificates: []tls.Certificate{cert},
	}
	tr := &http.Transport{
		TLSClientConfig: tlsConfig,
	}
	client := &http.Client{Transport: tr, Timeout: 1 * time.Minute}
	return client, nil
}

func main() {
	config, err := clientcmd.LoadFromFile("/home/dipankar/.kube/config")
	if err != nil {
		log.Fatalf("Failed to load kubeconfig file: %v", err)
	}

	cluster := config.Clusters["kind-kind"]
	usr := config.AuthInfos["kind-kind"]

	kubeapiURL := cluster.Server

	if _err := Write(LocationCaCert, cluster.CertificateAuthorityData); _err != nil {
		panic(_err)
	}

	if _err := Write(LocationClientCert, usr.ClientCertificateData); _err != nil {
		panic(_err)
	}

	if _err := Write(LocationClientKey, usr.ClientKeyData); _err != nil {
		panic(_err)
	}

	req, err := http.NewRequest(http.MethodGet, kubeapiURL+"/api/v1/namespaces/default/services/abcd:80/proxy/", nil)
	if err != nil {
		fmt.Printf("client: could not create request: %s\n", err)
		os.Exit(1)
	}

	client, err := HttpClient()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	res, err := client.Do(req)
	if err != nil {
		fmt.Printf("client: error making http request: %s\n", err)
		os.Exit(1)
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	fmt.Println("Response:", string(body))
}
