package main

type RoleConsts int

const (
	ROLE_CP RoleConsts = 0xff0
	ROLE_WP RoleConsts = 0xff1
	ROLE_DS RoleConsts = 0xff2
	ROLE_LB RoleConsts = 0xff3
)

func (role RoleConsts) String() string {
	switch role {
	case ROLE_CP:
		return "controlplane"
	case ROLE_DS:
		return "datastore"
	case ROLE_WP:
		return "workerplane"
	default:
		return ""
	}
}

type CloudConsts int

const (
	CLOUD_AWS   CloudConsts = 0xfa0
	CLOUD_GCP   CloudConsts = 0xfa1
	CLOUD_AZURE CloudConsts = 0xfa2
	CLOUD_CIVO  CloudConsts = 0xfa3
)

func (cloud CloudConsts) String() string {
	switch cloud {
	case CLOUD_AWS:
		return "aws"
	case CLOUD_AZURE:
		return "azure"
	case CLOUD_CIVO:
		return "civo"
	case CLOUD_GCP:
		return "gcp"
	default:
		return ""
	}
}

type Ksctl interface {
	Name(string) Ksctl
	Role(RoleConsts) Ksctl
	New(int) error
	Del(int) error
}

type params struct {
	label string
	id    string
}

type CloudClient interface {
	NewVM(string) (params, error)
	DelVM(string) error
}
