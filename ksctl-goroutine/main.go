package main

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	mrand "math/rand"
	"os"
	"sync"
	"time"
)

const (
	NO_RESOURCES = 2
)

type CloudCivo struct {
	Cloud  CloudConsts
	client CloudClient
	metadata
	mx sync.Mutex
}

type metadata struct {
	resName string
	role    RoleConsts
}
type state struct {
	Res  []string `json:"resources_ids"`
	Role []string `json:"resources_roles"`
}

var stateStore *state

func (m metadata) String() string {
	return fmt.Sprintf("{ Resource Name: '%s', Role: '%s' }", m.resName, m.role.String())
}

func (c *CloudCivo) Name(label string) Ksctl {

	c.metadata.resName = label
	fmt.Println("[trigger] Name", c.metadata)
	return c
}

func (c *CloudCivo) Role(role RoleConsts) Ksctl {

	c.metadata.role = role
	fmt.Println("[trigger] Role", c.metadata)
	return c
}

func save(path string) error {
	b, err := json.Marshal(stateStore)
	if err != nil {
		return err
	}
	return os.WriteFile(path, b, 0755)
}

func load(path string) error {
	var s *state
	b, err := os.ReadFile("cloud-state.json")
	if err != nil {
		return err
	}
	err = json.Unmarshal(b, &s)
	if err != nil {
		return err
	}
	stateStore = s
	return nil
}

func (c *CloudCivo) Del(serialNo int) error {

	fmt.Println("[trigger] Del")
	done := make(chan struct{})
	var errNewVM error
	go func() {
		errNewVM = c.client.DelVM(stateStore.Res[serialNo])
		if errNewVM != nil {
			close(done)
			return
		}

		c.mx.Lock()
		defer c.mx.Unlock()

		stateStore.Res[serialNo] = ""
		stateStore.Role[serialNo] = ""

		errNewVM = save("cloud-state.json")
		fmt.Println("[save]")
		close(done)
	}()
	<-done

	return errNewVM
}

func (c *CloudCivo) New(serialNo int) error {

	fmt.Println("[trigger] New")

	done := make(chan struct{})
	var errNewVM error
	go func() {
		role := c.metadata.role.String()
		var data params
		data, errNewVM = c.client.NewVM(c.metadata.resName)
		if errNewVM != nil {
			close(done)
			return
		}

		c.mx.Lock()
		defer c.mx.Unlock()

		stateStore.Res[serialNo] = data.id
		stateStore.Role[serialNo] = role

		errNewVM = save("cloud-state.json")
		fmt.Println("[save]")
		close(done)
	}()
	<-done

	return errNewVM
}

type civoGoClient struct {
	region string
}

func (c *civoGoClient) NewVM(label string) (params, error) {
	time.Sleep(time.Duration(mrand.Intn(5)) * time.Second)

	fmt.Printf("[client] label: %s\tregion: %s\n", label, c.region)
	return params{label: label, id: generateRandomString(10)}, nil
}

func (c *civoGoClient) DelVM(id string) error {
	time.Sleep(time.Duration(mrand.Intn(5)) * time.Second)
	return nil
}

func generateRandomString(length int) string {
	b := make([]byte, length)
	_, err := rand.Read(b)
	if err != nil {
		panic(err)
	}
	return base64.StdEncoding.EncodeToString(b)
}

func CloudFactory(cloud CloudConsts) Ksctl {
	switch cloud {
	case CLOUD_CIVO:
		return &CloudCivo{Cloud: cloud, client: &civoGoClient{}}
	default:
		return nil
	}
}

func Init(op, count int) {
	switch op {
	case 0:
		stateStore = &state{}
		stateStore.Res = make([]string, count)
		stateStore.Role = make([]string, count)
	case 1:
		err := load("cloud-state.json")
		if err != nil {
			panic(err)
		}
	}
}

func CreateKsctl() {

	var ksctl Ksctl = CloudFactory(CLOUD_CIVO)
	Init(0, NO_RESOURCES)
	var wg sync.WaitGroup // Used to wait for all goroutines to finish

	for i := 0; i < NO_RESOURCES; i++ {
		wg.Add(1) // Increment the WaitGroup counter
		go func(i int) {
			defer wg.Done() // Decrement the WaitGroup counter when done
			fmt.Println(i)
			var role RoleConsts
			if i%2 == 0 {
				role = ROLE_WP
			} else {
				role = ROLE_CP
			}
			_ = ksctl.Name(fmt.Sprintf("demo-%d", i)).Role(role).New(i)
		}(i)
	}

	wg.Wait() // Wait for all goroutines to finish
}

func DeleteKsctl() {
	var ksctl Ksctl = CloudFactory(CLOUD_CIVO)
	Init(1, -1)
	var wg sync.WaitGroup // Used to wait for all goroutines to finish

	for i := 0; i < NO_RESOURCES; i++ {
		wg.Add(1) // Increment the WaitGroup counter
		go func(i int) {
			defer wg.Done() // Decrement the WaitGroup counter when done
			fmt.Println(i)
			_ = ksctl.Del(i)
		}(i)
	}

	wg.Wait() // Wait for all goroutines to finish
	os.Remove("cloud-state.json")
}

func main() {
	args := os.Args

	if len(args) == 2 {
		switch args[1] {
		case "create":
			CreateKsctl()
		case "delete":
			DeleteKsctl()
		}
	}
}
