package main

import (
	"context"
	"fmt"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/clientcmd/api"
)

func main() {
	r := []byte(`
<Paste your kubeconfig here>
`)
	fmt.Println(string(r))

	// x, err := clientcmd.Load(r)
	// x, err := clientcmd.LoadFromFile("/home/dipankar/.kube/config")

	config, err := clientcmd.BuildConfigFromKubeconfigGetter("", func() (v *api.Config, err error) {
		return clientcmd.Load(r)
	})
	if err != nil {
		panic(err)
	}
	c, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err)
	}
	nodes, err := c.CoreV1().Nodes().List(context.Background(), v1.ListOptions{})
	if err != nil {
		panic(err)
	}

	for _, node := range nodes.Items {
		fmt.Println(node.Name)
	}
}
