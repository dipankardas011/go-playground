package main

import (
	"os"

	"github.com/civo/civogo"
)

type CivoGoClient struct {
	client    *civogo.Client
	region    string
	networkId string
}

func (client *CivoGoClient) InitClient() (err error) {
	client.client, err = civogo.NewClient(os.Getenv("CIVO_TOKEN"), os.Getenv("CIVO_REGION"))
	if err != nil {
		return
	}
	client.region = os.Getenv("CIVO_REGION")
	client.networkId = os.Getenv("CIVO_NETWORK_ID")
	return
}

func main() {
	// Create the civo loadbalanacer
	cloudClient := &CivoGoClient{}
	if err := cloudClient.InitClient(); err != nil {
		panic(err)
	}

	_, err := cloudClient.client.DeleteLoadBalancer(os.Getenv("LB_ID"))
	if err != nil {
		panic(err)
	}

}
