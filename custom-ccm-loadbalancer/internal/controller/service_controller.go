/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"

	"github.com/fatih/color"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
)

// ServiceReconciler reconciles a Service object
type ServiceReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

const serviceFinalizer = "ksctl.kubernetes.io/ccm-lb"

//+kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=configmap,verbs=get;list;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=services/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=core,resources=services/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Service object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.17.0/pkg/reconcile

func (r *ServiceReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := log.FromContext(ctx).V(-1).WithName("ksctl-ccm-lb")

	service := &corev1.Service{}

	color.HiYellow("TRIGGERED")

	if err := r.Get(ctx, req.NamespacedName, service); err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	if service.Spec.Type != corev1.ServiceTypeLoadBalancer {
		color.HiGreen("Skipped!")
		return ctrl.Result{}, nil
	}

	if service.DeletionTimestamp.IsZero() {
		// Service is not being deleted
		if !containsString(service.ObjectMeta.Finalizers, serviceFinalizer) {

			color.HiCyan("adding Finalizer")

			service.ObjectMeta.Finalizers = append(service.ObjectMeta.Finalizers, serviceFinalizer)

			if err := r.Update(context.Background(), service); err != nil {
				return ctrl.Result{}, err
			}
		} else {

			color.HiCyan("calling cloud provider client to create LB")

			if v, err := r.CreateLB(ctx, log, service); err != nil {
				return v, err
			}

			// if err := r.Update(context.Background(), service); err != nil {
			// 	return ctrl.Result{Requeue: true}, err
			// }
		}

	} else {
		// Service is being deleted
		if containsString(service.ObjectMeta.Finalizers, serviceFinalizer) {

			color.HiCyan("deprovisioning the lb")
			// Here you can capture the manifest or perform specific cleanup before final deletion
			if err := r.DeleteLB(ctx, log, service); err != nil {
				// need to requeue as there can be errors from cloud api
				return ctrl.Result{}, err
			}

			// Remove the finalizer to allow deletion to proceed
			color.HiCyan("removing Finalizer")
			service.ObjectMeta.Finalizers = removeString(service.ObjectMeta.Finalizers, serviceFinalizer)
			if err := r.Update(context.Background(), service); err != nil {
				return ctrl.Result{}, err
			}
		}
	}

	return ctrl.Result{}, nil

}

// Helper functions to manage finalizers
func containsString(slice []string, s string) bool {
	for _, item := range slice {
		if item == s {
			return true
		}
	}
	return false
}

func removeString(slice []string, s string) (result []string) {
	for _, item := range slice {
		if item != s {
			result = append(result, item)
		}
	}
	return
}

// SetupWithManager sets up the controller with the Manager.
func (r *ServiceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&corev1.Service{}).
		Complete(r)
}
