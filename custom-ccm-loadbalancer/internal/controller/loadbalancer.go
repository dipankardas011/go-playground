package controller

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/civo/civogo"
	"github.com/fatih/color"
	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var (
	Data map[string]string = make(map[string]string) // TODO: move to a more reliable way of storing the loadbalancerID
)

type CivoGoClient struct {
	client    *civogo.Client
	region    string
	networkId string
}

func (client *CivoGoClient) InitClient() (err error) {
	client.client, err = civogo.NewClient(os.Getenv("CIVO_TOKEN"), os.Getenv("CIVO_REGION"))
	if err != nil {
		return
	}
	client.region = os.Getenv("CIVO_REGION")
	client.networkId = os.Getenv("CIVO_NETWORK_ID")
	return
}

func BeautifulPrint(v any) {
	d, _ := json.MarshalIndent(v, "", " ")

	fmt.Println(string(d))
}

func (r *ServiceReconciler) CreateLB(ctx context.Context, log logr.Logger, service *corev1.Service) (ctrl.Result, error) {

	log.Info("Working on cloud provider")
	if _, ok := Data[GiveMeName(service)]; ok {
		return ctrl.Result{}, nil
	}

	hostPorts := []civogo.LoadBalancerBackendConfig{}
	for _, ports := range service.Spec.Ports {
		hostPorts = append(hostPorts, civogo.LoadBalancerBackendConfig{
			Protocol:   string(ports.Protocol),
			SourcePort: ports.Port,
			TargetPort: ports.NodePort,
		})
	}
	fmt.Println("nodePort", hostPorts)

	nodes := &corev1.NodeList{}
	if err := r.List(ctx, nodes); err != nil {
		// log.Error(err, "unable to fetch Node")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	finalBackend := []civogo.LoadBalancerBackendConfig{}

	for _, node := range nodes.Items {

		if _, ok := node.Labels["node-role.kubernetes.io/control-plane"]; !ok {
			continue
		}
		for _, addresses := range node.Status.Addresses {
			if addresses.Type == corev1.NodeInternalIP || addresses.Type == corev1.NodeExternalIP {
				temp := make([]civogo.LoadBalancerBackendConfig, len(hostPorts))
				copy(temp, hostPorts)
				for i := range temp {
					temp[i].IP = addresses.Address
				}

				finalBackend = append(finalBackend, temp...)
			}
		}
	}

	// Create the civo loadbalanacer
	cloudClient := &CivoGoClient{}
	if err := cloudClient.InitClient(); err != nil {
		return ctrl.Result{}, err
	}

	config := &civogo.LoadBalancerConfig{
		Name:                  GiveMeName(service),
		Region:                cloudClient.region,
		Algorithm:             "round_robin",
		ExternalTrafficPolicy: "Cluster",
		NetworkID:             cloudClient.networkId,
		Backends:              finalBackend,
	}

	BeautifulPrint(config)

	lb, err := cloudClient.client.CreateLoadBalancer(config)
	if err != nil {
		if !errors.Is(err, civogo.DatabaseLoadBalancerExistsError) && !errors.Is(err, civogo.DatabaseLoadBalancerDuplicateError) {
			return ctrl.Result{}, err
		} else {
			lb, err = cloudClient.client.FindLoadBalancer(config.Name)
			if err != nil {
				return ctrl.Result{}, err
			}
		}
	}

	fmt.Println("Data_of_lb:", Data)

	BeautifulPrint(lb)
	stateOfLb := ""
	publicIP := ""
	for stateOfLb != "available" {
		color.Red("trying to get the publicIP")
		lbw, err := cloudClient.client.GetLoadBalancer(lb.ID)
		if err != nil {
			return ctrl.Result{}, err
		}

		BeautifulPrint(lbw)

		stateOfLb = lbw.State
		publicIP = lbw.PublicIP

		time.Sleep(2 * time.Second)
	}
	fmt.Println("PublicIP of lb", publicIP)

	fmt.Println(service.Spec.ExternalIPs)

	service.Spec.ExternalIPs = append(service.Spec.ExternalIPs, publicIP)
	// service.Spec.LoadBalancerIP = publicIP

	if err := r.Update(context.Background(), service); err != nil {
		return ctrl.Result{Requeue: true}, err
	}

	Data[GiveMeName(service)] = lb.ID
	color.HiGreen("Done with cloud part")

	return ctrl.Result{}, nil
}

func GiveMeName(service *corev1.Service) string {
	return service.Namespace + "-" + service.Name
}

// func (r *ServiceReconciler) StoreData(ctx context.Context, v map[string]string, name string, namespace string) error {
//
// 	cm := &corev1.ConfigMap{}
//
// 	if err := r.Client.Get(ctx, types.NamespacedName{Namespace: namespace, Name: name}, cm); err != nil {
// 		if errors.IsNotFound(err) {
//
// 			cm := &corev1.ConfigMap{
// 				ObjectMeta: v1.ObjectMeta{Name: name, Namespace: namespace},
// 			}
// 			if err := r.Client.Create(ctx, cm); err != nil {
// 				return err
// 			}
// 		}
// 	}
//
// 	return nil
// }

func (r *ServiceReconciler) DeleteLB(ctx context.Context, log logr.Logger, service *corev1.Service) error {

	log.Info("Service is being deleted", "service", service)
	if _, ok := Data[GiveMeName(service)]; !ok {
		return nil
	}
	// Destroy the civo loadbalanacer

	cloudClient := &CivoGoClient{}
	if err := cloudClient.InitClient(); err != nil {
		return err
	}

	log.Info("Client ready to do magic")

	_, err := cloudClient.client.DeleteLoadBalancer(Data[GiveMeName(service)])
	if err != nil {
		return err
	}
	delete(Data, GiveMeName(service))

	color.HiGreen("Done with cloud part")
	return nil
}
