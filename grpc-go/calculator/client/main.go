package main

import (
	// "crypto/tls"

	"calculator/pb"
	"context"
	"log/slog"
	"time"

	"google.golang.org/grpc"

	// "google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	// creds := credentials.NewTLS(&tls.Config{InsecureSkipVerify: false})

	opts := []grpc.DialOption{
		// grpc.WithTransportCredentials(creds),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	}

	url := "127.0.0.1:8080"

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	conn, err := grpc.DialContext(ctx, url, opts...)
	if err != nil {
		slog.Error("fail to dial", "reason", err)
	}
	defer conn.Close()

	client := pb.NewCalculatorClient(conn)

	res, err := client.Sum(ctx, &pb.NumberReq{Numbers: []int64{10, 10}})
	if err != nil {
		slog.Error("failed to do sum", "reason", err)
	}
	slog.Info("Result", "sum", res.Res)
}
