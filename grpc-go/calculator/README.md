```bash
# protoCurl
grpcurl -plaintext -d '{"numbers": [3,4,3]}' 127.0.0.1:8080 calculator.Calculator.Sum

grpcurl -plaintext -d '{"a": 2, "b": 1}' 127.0.0.1:8080 calculator.Calculator.Div
```
