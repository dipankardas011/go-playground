package main

import (
	"calculator/pb"
	"context"
	"net"

	"log/slog"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
)

type server struct {
	pb.UnimplementedCalculatorServer
}

func (s *server) Add(ctx context.Context, in *pb.CalReq) (*pb.CalRes, error) {
	return &pb.CalRes{
		Res: in.A + in.B,
	}, nil
}

func (s *server) Div(ctx context.Context, in *pb.CalReq) (*pb.CalRes, error) {

	if in.B == 0 {
		return nil, status.Error(codes.InvalidArgument, "divisor cannnot be zero")
	}

	return &pb.CalRes{
		Res: in.A / in.B,
	}, nil
}

func (s *server) Sum(ctx context.Context, in *pb.NumberReq) (*pb.CalRes, error) {

	var ret int64

	for _, v := range in.Numbers {
		ret += v
	}

	return &pb.CalRes{Res: ret}, nil
}

func main() {
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		slog.Error("unable to do http listener", "err", err)
	}

	s := grpc.NewServer()
	reflection.Register(s) // for debugging purposes

	pb.RegisterCalculatorServer(s, &server{}) // Register the server with the gRPC server

	slog.Info("Server started", "port", "8080")

	if err := s.Serve(listener); err != nil {
		slog.Error("failed to serve", "err", err)
	}
}
