package main

import (
	"context"
	"log"
	"net"
	"strconv"
	"time"

	pb "try-grpc-go/template" // Import the generated Go package

	"google.golang.org/grpc"
)

type server struct {
	pb.UnimplementedGreeterServer
}

func (s *server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {
	log.Println(in.Name)
	return &pb.HelloReply{
		Message: "Hello, " + in.Name,
	}, nil
}

func (s *server) StreamHello(req *pb.HelloRequest, stream pb.Greeter_StreamHelloServer) error {
	name := req.GetName()

	for i := 0; i < 5; i++ {
		message := "Hello, " + name + ", this is message number " + strconv.Itoa(i+1)
		reply := &pb.HelloReply{Message: message}

		log.Println(reply.String())
		if err := stream.Send(reply); err != nil {
			return err
		}
		time.Sleep(time.Second) // Simulate some processing time between messages
	}

	return nil
}

func main() {
	lis, err := net.Listen("tcp", ":50051") // gRPC server will listen on port 50051
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterGreeterServer(s, &server{}) // Register the server with the gRPC server
	log.Println("Server started on port 50051...")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
