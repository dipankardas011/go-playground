
const grpc = require('grpc');

const protoLoader = require('@grpc/proto-loader');

const packageDefinition = protoLoader.loadSync('template/Demo.proto', {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
});
const protoDescriptor = grpc.loadPackageDefinition(packageDefinition);

const client = new protoDescriptor.template.Greeter('localhost:50051', grpc.credentials.createInsecure());

const name = 'John-JS client';

client.sayHello({ name }, (error, response) => {
    if (!error) {
        console.log('Response from server:', response.message);
    } else {
        console.error('Error:', error);
    }
});


const request = { name: 'John' };

// Call the streaming RPC
const call = client.streamHello(request);

// Handle streaming responses
call.on('data', (response) => {
    console.log(`Received: ${response.message}`);
});

call.on('end', () => {
    console.log('Server streaming ended.');
});

call.on('error', (error) => {
    console.error('Error:', error);
});
