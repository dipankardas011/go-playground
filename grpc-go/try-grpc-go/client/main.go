package main

import (
	"context"
	"io"
	"log"

	pb "try-grpc-go/template" // Import the generated Go package

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	// https://github.com/grpc/grpc-go/blob/master/examples/route_guide/client/client.go#L155
	conn, err := grpc.Dial("localhost:50051", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewGreeterClient(conn) // Create a new Greeter client

	// Call the SayHello RPC
	name := "John-Go client"
	r, err := c.SayHello(context.Background(), &pb.HelloRequest{Name: name})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Response from server: %s", r.Message)

	// Call the streaming RPC
	stream, err := c.StreamHello(context.Background(), &pb.HelloRequest{Name: name})
	if err != nil {
		log.Fatalf("error while calling StreamHello: %v", err)
	}
	// Receive messages from the server stream
	for {
		reply, err := stream.Recv()

		if err == io.EOF {
			// The stream has ended, exit the loop
			break
		}
		if err != nil {
			log.Fatalf("error while receiving response: %v", err)
		}
		log.Printf("Received: %s", reply.Message)
	}
}
