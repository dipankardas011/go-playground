package main

import (
	"context"
	"log/slog"
	"os"
	"path"
	"strings"
	"time"

	"github.com/hashicorp/vault-client-go"
)

func main() {
	ctx := context.Background()

	client, err := vault.New(
		vault.WithAddress(os.Getenv("VAULT_ADDR")),
		vault.WithRequestTimeout(30*time.Second),
	)
	if err != nil {
		slog.Error("Error", err)
	}

	usr, err := os.UserHomeDir()
	if err != nil {
		slog.Error("Error", err)
	}
	locPath := path.Join(usr, ".vault")
	slog.Info(locPath)

	v, err := os.ReadFile(locPath)
	if err != nil {
		slog.Error("Error", err)
	}

	if err := client.SetToken(strings.TrimRight(string(v), "\n")); err != nil {
		slog.Error("Error", err)
	}

	s, err := client.Secrets.KvV2Read(ctx, os.Getenv("VAULT_STORE"), vault.WithMountPath("kv"))
	if err != nil {
		slog.Error("Error", err)
	}
	slog.Info("secret retrieved:", "map[]", s.Data.Data)
}
