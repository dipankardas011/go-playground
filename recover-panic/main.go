package main

import (
	"log/slog"
	"time"
)

type Failing struct {
}

type Logger interface {
	V(level int)
	Info(msg string)
	Warn(msg string)
}

func (f *Failing) V(level int) {
}

func (f *Failing) Info(msg string) {
	slog.Info(msg)
}

func (f *Failing) Warn(msg string) {
	slog.Warn(msg)
}

func mustPanic() {

	defer func() {
		if r := recover(); r != nil {
			slog.Error("Recovered from", "r", r)
		}
	}()
	var log Logger
	log.Info("Hi")
}

func mustAlgetra() int {
	defer func() {
		if r := recover(); r != nil {
			slog.Error("Recovered from", "r", r)
		}
	}()
	a := 0
	time.Sleep(2 * time.Second)
	b := 5
	return b / a
}

func main() {
	mustPanic()
	_ = mustAlgetra()
}
