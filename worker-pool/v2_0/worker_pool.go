package v1_0

import (
	"fmt"
	"sync"
)

type Task interface {
	Process()
}

type WorkerPool struct {
	Tasks       []Task
	Concurrency int
	tasksChan   chan Task

	wg sync.WaitGroup
}

func (w *WorkerPool) worker() {
	for task := range w.tasksChan {
		task.Process()
		w.wg.Done()
	}
}

func (w *WorkerPool) Run() {
	w.tasksChan = make(chan Task, len(w.Tasks))

	for i := 0; i < w.Concurrency; i++ {
		go w.worker()
	}

	w.wg.Add(len(w.Tasks))

	for _, task := range w.Tasks {
		w.tasksChan <- task
	}

	close(w.tasksChan) // for signalling now more tasks

}

func (w *WorkerPool) Close() {
	fmt.Println("Waiting for all go routines to stop")
	w.wg.Wait()
	fmt.Println("Completed all tasks")
}
