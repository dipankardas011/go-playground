package main

import (
	"fmt"
	"time"
	workerPoolV2 "worker-pool/v2_0"
)

type EmailTask struct {
	Addr string
	Body string
	Subj string
}

func (e *EmailTask) Process() {
	fmt.Printf("Sending an email Addr: %s | Subj: %s | Body: %s\n", e.Addr, e.Subj, e.Body)

	time.Sleep(2 * time.Second)
}

type ImageProcessingTask struct {
	ImgUrl string
}

func (i *ImageProcessingTask) Process() {
	fmt.Printf("Processing an image with URL: %s\n", i.ImgUrl)

	time.Sleep(5 * time.Second)
}

func main() {
	tasks := []workerPoolV2.Task{
		&EmailTask{Addr: "example1@xyz.com", Subj: "11", Body: "hello world"},
		&ImageProcessingTask{ImgUrl: "/abcd/1.png"},

		&EmailTask{Addr: "example2@xyz.com", Subj: "22", Body: "hello world"},
		&ImageProcessingTask{ImgUrl: "/abcd/2.png"},

		&EmailTask{Addr: "example3@xyz.com", Subj: "33", Body: "hello world"},
		&ImageProcessingTask{ImgUrl: "/abcd/3.png"},

		&EmailTask{Addr: "example4@xyz.com", Subj: "44", Body: "hello world"},
		&ImageProcessingTask{ImgUrl: "/abcd/4.png"},

		&EmailTask{Addr: "example5@xyz.com", Subj: "55", Body: "hello world"},
		&ImageProcessingTask{ImgUrl: "/abcd/5.png"},

		&EmailTask{Addr: "example6@xyz.com", Subj: "66", Body: "hello world"},
		&ImageProcessingTask{ImgUrl: "/abcd/6.png"},

		&EmailTask{Addr: "example7@xyz.com", Subj: "77", Body: "hello world"},
		&ImageProcessingTask{ImgUrl: "/abcd/7.png"},

		&EmailTask{Addr: "example8@xyz.com", Subj: "88", Body: "hello world"},
		&ImageProcessingTask{ImgUrl: "/abcd/8.png"},

		&EmailTask{Addr: "example9@xyz.com", Subj: "99", Body: "hello world"},
		&ImageProcessingTask{ImgUrl: "/abcd/9.png"},

		&EmailTask{Addr: "example10@xyz.com", Subj: "10", Body: "hello world"},
		&ImageProcessingTask{ImgUrl: "/abcd/10.png"},
	}

	wp := &workerPoolV2.WorkerPool{
		Tasks:       tasks,
		Concurrency: 5,
	}

	wp.Run()
	wp.Close()

	// ////////////////
	// tasks := make([]workerPoolV1.Task, 20)
	//
	//	for i := 0; i < 20; i++ {
	//		tasks[i] = workerPoolV1.Task{Id: i + 1}
	//	}
	//
	//	wp := &workerPoolV1.WorkerPool{
	//		Tasks:       tasks,
	//		Concurrency: 5,
	//	}
	//
	// wp.Run()
	// wp.Close()
}
