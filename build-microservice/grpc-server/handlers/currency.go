package handlers

import (
	"context"
	"grpc-server/data"
	protosCurrency "grpc-server/protos/gen/currency"
	"log"
	"time"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Currency struct {
	l    *log.Logger
	rate *data.ExchangeRates

	sigWhenToUnSubscribe map[protosCurrency.Currency_SubscribeRatesServer]chan struct{}

	subscriptions map[protosCurrency.Currency_SubscribeRatesServer][]*protosCurrency.RateRequest
	protosCurrency.UnimplementedCurrencyServer
}

func NewCurrency(r *data.ExchangeRates, l *log.Logger) *Currency {
	c := &Currency{
		l:                    l,
		rate:                 r,
		subscriptions:        make(map[protosCurrency.Currency_SubscribeRatesServer][]*protosCurrency.RateRequest),
		sigWhenToUnSubscribe: make(map[protosCurrency.Currency_SubscribeRatesServer]chan struct{}),
	}

	go c.handleSubscriptions()
	go c.handleCancelSubscription()

	return c
}

func (c *Currency) GetRate(
	ctx context.Context,
	in *protosCurrency.RateRequest) (*protosCurrency.RateResponse, error) {

	if in.GetBase() == in.GetDestination() {
		stat := status.Newf(
			codes.InvalidArgument,
			"base currency can't be the same as the destination currency",
		)
		stat, estat := stat.WithDetails(in)
		if estat != nil {
			return nil, estat
		}

		return nil, stat.Err()
	}

	c.l.Println("GetRate called", "base", in.GetBase(), "target", in.GetDestination())

	r, err := c.rate.GetRate(in.GetBase().String(), in.GetDestination().String())
	if err != nil {
		c.l.Println("failed to get rate", "err", err)
		stat := status.Newf(
			codes.InvalidArgument,
			err.Error(),
		)
		stat, estat := stat.WithDetails(in)
		if estat != nil {
			return nil, estat
		}
		return nil, stat.Err()
	}
	c.l.Println("rate", r)

	return &protosCurrency.RateResponse{
		Rate:        r,
		Base:        in.GetBase(),
		Destination: in.GetDestination(),
	}, nil
}

func (c *Currency) handleCancelSubscription() {
	c.l.Println("Subscription canceller handler started")

	for {
		for usr := range c.sigWhenToUnSubscribe {
			for range c.sigWhenToUnSubscribe[usr] {
				c.l.Println("Subscription cancelled")
				close(c.sigWhenToUnSubscribe[usr])
				delete(c.sigWhenToUnSubscribe, usr)
				delete(c.subscriptions, usr)
			}
		}
	}
}

func (c *Currency) handleSubscriptions() {
	for range c.rate.MonitorRates(15 * time.Second) {
		for sub, reqs := range c.subscriptions {
			for _, req := range reqs {

				if r, err := c.rate.GetRate(
					req.GetBase().String(),
					req.GetDestination().String(),
				); err != nil {

					c.l.Println("failed to get rate", "err", err)

				} else {

					if err := sub.Send(
						&protosCurrency.StreamingRateResponse{
							Message: &protosCurrency.StreamingRateResponse_Rate{
								Rate: &protosCurrency.RateResponse{
									Rate:        r,
									Base:        req.GetBase(),
									Destination: req.GetDestination(),
								},
							},
						},
					); err != nil {
						c.l.Println("failed to send rate", "err", err)
					}
				}
			}
		}
	}
}

func (c *Currency) SubscribeRates(
	req_res protosCurrency.Currency_SubscribeRatesServer,
) error {
	c.l.Println("SubscribeRates called")

	for {
		req, err := req_res.Recv()

		if status.Code(err) == codes.Canceled {
			c.l.Println("status code is context cancelled")
			c.sigWhenToUnSubscribe[req_res] <- struct{}{}
			break
		}

		if err != nil {
			c.l.Println("failed to receive", "err", err)
			c.sigWhenToUnSubscribe[req_res] <- struct{}{}
			return err
		}

		sub, ok := c.subscriptions[req_res]
		if !ok {
			sub = []*protosCurrency.RateRequest{}
		}

		var validationErr *status.Status
		for _, v := range sub {
			if v.Base == req.Base && v.Destination == req.Destination {
				validationErr, err = status.Newf(
					codes.AlreadyExists,
					"Already subscribed to the rate",
				).WithDetails(req)
				if err != nil {
					c.l.Println("unable to add metadata", "err", err)
					break
				}

				break
			}
		}

		if validationErr != nil {
			req_res.Send(
				&protosCurrency.StreamingRateResponse{
					Message: &protosCurrency.StreamingRateResponse_Error{
						Error: validationErr.Proto(),
					},
				},
			)
			continue
		}

		sub = append(sub, req)
		c.subscriptions[req_res] = sub

		csub, ok := c.sigWhenToUnSubscribe[req_res]
		if !ok {
			csub = make(chan struct{})
		}
		c.sigWhenToUnSubscribe[req_res] = csub

		c.l.Println("Subscribed to Rates", "base", req.GetBase().String(), "target", req.GetDestination().String())
	}
	return nil
}
