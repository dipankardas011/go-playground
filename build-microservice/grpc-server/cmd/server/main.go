package main

import (
	"grpc-server/data"
	"grpc-server/handlers"
	"log"
	"net"
	"os"

	protosCurrency "grpc-server/protos/gen/currency"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	log := log.New(os.Stdout, "[grpc] ", log.LstdFlags|log.Lshortfile)

	rs, err := data.NewExchangeRates(log)
	if err != nil {
		log.Fatal(err)
	}

	cs := handlers.NewCurrency(rs, log)

	gs := grpc.NewServer()

	reflection.Register(gs) // WARN: always remove this line in production

	protosCurrency.RegisterCurrencyServer(gs, cs)

	lis, err := net.Listen("tcp", ":9090")
	if err != nil {
		panic(err)
	}
	log.Println("Starting server on :9090")
	if err := gs.Serve(lis); err != nil {
		panic(err)
	}
}
