package data

import (
	"log"
	"os"
	"testing"
)

func TestNewRates(t *testing.T) {
	if tr, err := NewExchangeRates(log.New(os.Stdout, "rates-test", log.LstdFlags)); err != nil {
		t.Error(err)
	} else {
		t.Logf("%#+v", tr)
	}
}
