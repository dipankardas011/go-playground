package data

import (
	"encoding/xml"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"math/rand"
)

type ExchangeRates struct {
	l     *log.Logger
	rates map[string]float64
}

func NewExchangeRates(l *log.Logger) (*ExchangeRates, error) {
	e := &ExchangeRates{l: l, rates: map[string]float64{}}
	err := e.getRates()
	if err != nil {
		return nil, err
	}
	return e, nil
}

func (r *ExchangeRates) GetRate(base, dest string) (float64, error) {
	baseRate, ok := r.rates[base]
	if !ok {
		return 0, fmt.Errorf("base currency %s not found", base)
	}
	destRate, ok := r.rates[dest]
	if !ok {
		return 0, fmt.Errorf("destination currency %s not found", dest)
	}
	return destRate / baseRate, nil
}

// MonitorRates checks the rates in the ECB API every interval and sends a message to the
// returned channel when there are changes
//
// Note: the ECB API only returns data once a day, this function only simulates the changes
// in rates for demonstration purposes
func (e *ExchangeRates) MonitorRates(interval time.Duration) chan struct{} {
	ret := make(chan struct{})

	go func() {
		ticker := time.NewTicker(interval)
		for {
			select {
			case <-ticker.C:
				// just add a random difference to the rate and return it
				// this simulates the fluctuations in currency rates
				for k, v := range e.rates {
					// change can be 10% of original value
					change := (rand.Float64() / 10)
					// is this a postive or negative change
					direction := rand.Intn(1)

					if direction == 0 {
						// new value with be min 90% of old
						change = 1 - change
					} else {
						// new value will be 110% of old
						change = 1 + change
					}

					// modify the rate
					e.rates[k] = v * change
				}

				// notify updates, this will block unless there is a listener on the other end
				ret <- struct{}{}
			}
		}
	}()

	return ret
}

func (e *ExchangeRates) getRates() error {
	resp, err := http.Get("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml")
	if err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("expected status 200 but got %d", resp.StatusCode)
	}

	defer resp.Body.Close()

	rates := Cubes{}
	err = xml.NewDecoder(resp.Body).Decode(&rates)
	if err != nil {
		return err
	}
	for _, c := range rates.Cube {
		r, err := strconv.ParseFloat(c.Rate, 64)
		if err != nil {
			return err
		}
		e.rates[c.Currency] = r
	}
	e.rates["EUR"] = 1.0

	return nil
}

type Cubes struct {
	Cube []Cube `xml:"Cube>Cube>Cube"`
}

type Cube struct {
	Currency string `xml:"currency,attr"`
	Rate     string `xml:"rate,attr"`
}
