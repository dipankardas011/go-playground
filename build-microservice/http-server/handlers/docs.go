// Package classification of Product API
//
// Documentation for Product API
//
//	Schemes: http
//	BasePath: /
//	Version: 1.0.0
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
// swagger:meta
package handlers

import "http-server/data"

// A list of products returns in the response
// swagger:response productsResponse
type productsResponseWrapper struct {
	// All products in the system
	// in: body
	Body []data.Product
}

// No content is returned by this API endpoint
// swagger:response noContent
type noContent struct {
}

// swagger:parameters listProducts getProduct
type productQueryParameterWrapper struct {
	// The currency for the product. By default its EUR
	// in: query
	// required: false
	Currency string `json:"currency"`
}

// swagger:parameters deleteProduct getProduct updateProduct
type productIDParameterWrapper struct {
	// The id of the product from the database
	// in: path
	// required: true
	ID int `json:"id"`
}

// swagger:parameters updateProduct createProduct
type productUpdateParameterWrapper struct {
	// in: body
	// required: true
	Body data.Product
}

// Data structure representing a single product
// swagger:response productResponse
type productResponseWrapper struct {
	// Newly created product
	// in: body
	Body data.Product
}

// Generic error message returned as a string
// swagger:response errorResponse
type errorResponseWrapper struct {
	// Description of the error
	// in: body
	Body GenericError
}

// Validation errors defined as an array of strings
// swagger:response errorValidation
type errorValidationWrapper struct {
	// Collection of the errors
	// in: body
	Body ValidationError
}
