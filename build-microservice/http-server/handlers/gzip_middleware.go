package handlers

import (
	"compress/gzip"
	"net/http"
	"strings"
)

type GzipMiddleware struct {
}

func NewGzipMiddleware() *GzipMiddleware {
	return &GzipMiddleware{}
}

func (h *GzipMiddleware) GetMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			if strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
				grw := NewWrappedResponsedWriter(rw)

				grw.Header().Set("Content-Encoding", "gzip")
				next.ServeHTTP(grw, r)
				defer grw.Flush()
				return
			}

			next.ServeHTTP(rw, r)
		})
}

type WrappedResponsedWriter struct {
	rw http.ResponseWriter
	gw *gzip.Writer
}

func NewWrappedResponsedWriter(rw http.ResponseWriter) *WrappedResponsedWriter {
	return &WrappedResponsedWriter{
		rw: rw,
		gw: gzip.NewWriter(rw),
	}
}

func (wrw *WrappedResponsedWriter) Header() http.Header {
	return wrw.rw.Header()
}

func (wrw *WrappedResponsedWriter) Write(data []byte) (int, error) {
	return wrw.gw.Write(data)
}

func (wrw *WrappedResponsedWriter) WriteHeader(statusCode int) {
	wrw.rw.WriteHeader(statusCode)
}

func (wrw *WrappedResponsedWriter) Flush() {
	wrw.gw.Flush()
	wrw.gw.Close()
}
