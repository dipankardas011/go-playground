package handlers

import (
	"http-server/files"
	"log"
	"net/http"
	"path/filepath"
	"strconv"

	"github.com/gorilla/mux"
)

type FileServer struct {
	l *log.Logger
	f files.FileSystem
}

func NewFileServer(l *log.Logger, f files.FileSystem) *FileServer {
	return &FileServer{l, f}
}

func (fSvr *FileServer) HandlePostREST(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	filePath := vars["filename"]
	loc := filepath.Join(id, filePath)
	fSvr.l.Println("Handle POST", "id", id, "filename", filePath)
	if err := fSvr.f.Save(loc, r.Body); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (fSvr *FileServer) HandleMultipartForm(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(10 * 1024)
	if err != nil {
		http.Error(w, "Unable to parse form", http.StatusBadRequest)
		return
	}

	id := r.FormValue("id")

	if _, errId := strconv.Atoi(id); errId != nil {
		http.Error(w, "Unable to convert id", http.StatusBadRequest)
		return
	}

	file, info, err := r.FormFile("file")

	loc := filepath.Join(id, info.Filename)
	if err := fSvr.f.Save(loc, file); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte("Stored the file to loc:" + info.Filename))
}
