package handlers

import (
	"context"
	"errors"
	"http-server/data"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type Products struct {
	l  *log.Logger
	db *data.ProductDB
}

func NewProducts(l *log.Logger, db *data.ProductDB) *Products {
	return &Products{l, db}
}

// GenericError is a generic error message returned by a server
type GenericError struct {
	Message string `json:"message"`
}

// ValidationError is a collection of validation error messages
type ValidationError struct {
	Messages []string `json:"messages"`
}

// swagger:route GET /products products listProducts
// Returns a list of products
// Responses:
//
//	200: productsResponse
func (p *Products) GetProducts(w http.ResponseWriter, r *http.Request) {
	p.l.Println("Handle GET Products")

	curr := r.URL.Query().Get("currency")

	w.Header().Add("Content-Type", "application/json")
	products, err := p.db.GetProducts(curr)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := data.ToJSON(products, w); err != nil {
		http.Error(w, "Unable to marshal json", http.StatusInternalServerError)
	}
}

// swagger:route GET /product/{id} product getProduct
// Returns a product
// Responses:
//
//	201: productsResponse
func (p *Products) GetProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "Unable to convert id", http.StatusBadRequest)
		return
	}
	curr := r.URL.Query().Get("currency")

	w.Header().Add("Content-Type", "application/json")
	product, _, err := p.db.FindProduct(id, curr, true)
	if err != nil {
		if errors.Is(err, data.ErrProductNotFound) {
			http.Error(w, "Product not found", http.StatusNotFound)
		} else {
			http.Error(w, "Unable to get it", http.StatusInternalServerError)
		}
		return
	}

	products := data.Products{product}
	if err := data.ToJSON(products, w); err != nil {
		http.Error(w, "Unable to marshal json", http.StatusInternalServerError)
	}
}

// swagger:route POST /product product createProduct
// Create a new product
//
// responses:
//
// 201: noContent
// 422: errorValidation
// 501: errorResponse
func (p *Products) AddProducts(w http.ResponseWriter, r *http.Request) {
	p.l.Println("Handle POST Products")

	prod := r.Context().Value(ProductKey{}).(*data.Product)

	if err := p.db.AddProduct(prod, ""); err != nil {
		http.Error(w, "Unable to add product", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
}

// swagger:route PUT /product/{id} product updateProduct
// update a product
//
// responses:
//
// 201: noContent
// 422: errorValidation
// 501: errorResponse
func (p *Products) UpdateProduct(w http.ResponseWriter, r *http.Request) {

	prod := r.Context().Value(ProductKey{}).(*data.Product)

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "Unable to convert id", http.StatusBadRequest)
		return
	}

	if err := p.db.UpdateProduct(id, prod, ""); err != nil {
		if errors.Is(err, data.ErrProductNotFound) {
			http.Error(w, "Product not found", http.StatusNotFound)
		} else {
			http.Error(w, "Product not found", http.StatusInternalServerError)
		}
	}
}

// swagger:route DELETE /product/{id} products deleteProduct
// Returns a list of products
// Responses:
//
//	201: noContent
func (p *Products) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "Unable to convert id", http.StatusBadRequest)
		return
	}
	if err := p.db.DeleteProduct(id); err != nil {
		if errors.Is(err, data.ErrProductNotFound) {
			http.Error(w, "Product not found", http.StatusNotFound)
		} else {
			http.Error(w, "Product not found", http.StatusInternalServerError)
		}
	}
}

type ProductKey struct{}

func (p *Products) MiddlewareProductValidation(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		prod := new(data.Product)
		if err := prod.FromJSON(r.Body); err != nil {
			p.l.Println("[ERROR] deserializing product", err)
			w.WriteHeader(http.StatusBadRequest)
			data.ToJSON(&GenericError{Message: err.Error()}, w)
			return
		}
		if err := prod.Validate(); err != nil {
			p.l.Println("[ERROR] deserializing product", err)
			w.WriteHeader(http.StatusUnprocessableEntity)
			data.ToJSON(&GenericError{Message: err.Error()}, w)
			return
		}
		ctx := context.WithValue(r.Context(), ProductKey{}, prod)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
