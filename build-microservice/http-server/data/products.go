package data

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	protosCurrency "grpc-server/protos/gen/currency"
	"io"
	"log"
	"regexp"
	"slices"

	"github.com/go-playground/validator/v10"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// swagger:model
type Product struct {
	ID int `json:"id"`
	// the name for this product
	//
	// required: true
	Name string `json:"name" validate:"required"`
	Desc string `json:"description"`
	// the price for the product
	//
	// required: true
	// min: 0.01
	// max: 1000.00
	Price float64 `json:"price" validate:"gt=0,lt=1000,required"`
	// the SKU for this product
	//
	// required: true
	// pattern: [a-z]+-[a-z]+-[a-z]+
	SKU       string `json:"sku" validate:"required,sku"`
	UpdatedOn string `json:"-"`
}

func (p *Product) FromJSON(r io.Reader) error {
	e := json.NewDecoder(r)
	return e.Decode(p)
}

func (p *Product) Validate() error {
	validate := validator.New()
	validate.RegisterValidation("sku", validateSKU)
	return validate.Struct(p)
}

func validateSKU(fl validator.FieldLevel) bool {
	reg := regexp.MustCompile(`^[a-z]+-[a-z]+-[a-z]+$`)
	matches := reg.FindAllString(fl.Field().String(), -1)
	return len(matches) == 1
}

type Products []*Product

type ProductDB struct {
	currency           protosCurrency.CurrencyClient
	l                  *log.Logger
	cacheRates         map[string]float64
	subscriptionClient protosCurrency.Currency_SubscribeRatesClient
}

func NewProductsDB(l *log.Logger, cc protosCurrency.CurrencyClient) *ProductDB {
	pb := &ProductDB{cc, l, make(map[string]float64), nil}

	go pb.handleUpdates()

	return pb
}

func ToJSON(i any, w io.Writer) error {
	e := json.NewEncoder(w)
	return e.Encode(i)
}

func (p *ProductDB) handleUpdates() {
	sub, err := p.currency.SubscribeRates(context.Background())
	if err != nil {
		p.l.Println("Error subscribing to rates", err)
	}

	p.subscriptionClient = sub

	for {
		rr, err := sub.Recv()
		if err != nil {
			p.l.Println("Error receiving update", err)
			return
		}
		if subErr := rr.GetError(); subErr != nil {
			p.l.Println("Error subscribing for rates", subErr.Code, subErr.Message) // need to get the details
			continue
		}
		if resp := rr.GetRate(); resp != nil {
			p.l.Println("Recieved the updated rate from the server",
				"destination", resp.GetDestination().String(),
				"rate", resp.GetRate(),
			)

			p.cacheRates[resp.GetDestination().String()] = resp.Rate
		}
	}
}

func (p *ProductDB) FromCurrency(currency string) (float64, error) {
	_currency, ok := protosCurrency.Currencies_value[currency]
	if !ok {
		return 0.0, errors.New("Invalid currency")
	}

	rr := protosCurrency.RateRequest{
		Base:        protosCurrency.Currencies(_currency),
		Destination: protosCurrency.Currencies_EUR,
	}
	resp, err := p.currency.GetRate(context.Background(), &rr)
	if err != nil {
		return 0.0, err
	}
	return resp.Rate, nil
}

func (p *ProductDB) ToCurrency(destCurrency string) (float64, error) {
	_currency, ok := protosCurrency.Currencies_value[destCurrency]
	if !ok {
		return 0.0, errors.New("Invalid currency")
	}

	// NOTE: disabled this for the sake of seeing already existing rate which is subscribed
	// if _, ok := p.cacheRates[destCurrency]; ok {
	// 	return p.cacheRates[destCurrency], nil
	// }

	rr := protosCurrency.RateRequest{
		Base:        protosCurrency.Currencies_EUR,
		Destination: protosCurrency.Currencies(_currency),
	}

	// initial getrate
	resp, err := p.currency.GetRate(context.Background(), &rr)
	if err != nil {
		if s, ok := status.FromError(err); ok {
			md := s.Details()[0].(*protosCurrency.RateRequest)
			if s.Code() == codes.InvalidArgument {
				return 0.0, fmt.Errorf("[%s] %s. base: %s, dest: %s",
					s.Code().String(),
					s.Message(),
					md.GetBase().String(),
					md.GetDestination().String(),
				)
			}
			return 0.0, fmt.Errorf("Failed to get rate: %w", err)
		}
		return 0.0, err
	}
	p.cacheRates[destCurrency] = resp.Rate

	// subscribe for updates
	if err := p.subscriptionClient.Send(&rr); err != nil {
		return 0.0, fmt.Errorf("Failed to perform initial subscription of new rates: %w", err)
	}

	return resp.Rate, nil
}

func (p *ProductDB) GetProducts(currency string) (Products, error) {
	if currency == "" {
		return productList, nil
	}

	r, err := p.ToCurrency(currency)
	if err != nil {
		return nil, err
	}

	products := []*Product{}
	for _, product := range productList {
		_p := *product
		_p.Price *= r
		products = append(products, &_p)
	}

	return products, nil
}

func (pr *ProductDB) AddProduct(p *Product, currency string) error {
	p.ID = getNextID()
	if currency != "" {
		r, err := pr.FromCurrency(currency)
		if err != nil {
			return err
		}
		p.Price *= r
	}
	productList = append(productList, p)
	return nil
}

func (pr *ProductDB) UpdateProduct(id int, p *Product, currency string) error {
	_, pos, err := pr.FindProduct(id, currency, false)
	if err != nil {
		return err
	}
	p.ID = id
	productList[pos] = p
	return nil
}

var ErrProductNotFound = errors.New("Product not found")

func (pr *ProductDB) FindProduct(id int, currency string, isGet bool) (*Product, int, error) {
	for i, p := range productList {
		if p.ID == id {
			if currency == "" {
				return p, i, nil
			}

			var f func(string) (float64, error)
			if isGet {
				f = pr.ToCurrency
			} else {
				f = pr.FromCurrency
			}
			r, err := f(currency)
			if err != nil {
				return nil, -1, err
			}
			x := *p
			x.Price *= r
			return &x, i, nil
		}
	}
	return nil, -1, ErrProductNotFound
}

func (p *ProductDB) DeleteProduct(id int) error {
	_, pos, err := p.FindProduct(id, "", false)
	if err != nil {
		return err
	}
	productList = slices.Concat(productList[:pos], productList[pos+1:])
	return nil
}

func getNextID() int {
	lp := productList[len(productList)-1]
	return lp.ID + 1
}

var productList = []*Product{
	{
		ID:    1,
		Name:  "Latte",
		Desc:  "Frothy milky coffee",
		Price: 2.45,
		SKU:   "abc123",
	},
	{
		ID:    3,
		Name:  "Nice",
		Desc:  "dcvfsdertyuio",
		Price: 45.0,
		SKU:   "xsdcs123",
	},
}
