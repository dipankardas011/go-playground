package data

import (
	"testing"

	"gotest.tools/v3/assert"
)

func TestCheckValidation(t *testing.T) {
	p := &Product{
		Name:  "test",
		Price: 1.00,
		SKU:   "abc-abc-abc",
	}

	err := p.Validate()
	assert.NilError(t, err)
}
