package main

import (
	"fmt"
	"gotest.tools/v3/assert"
	"http-server/client"
	"http-server/client/products"
	"testing"
)

func TestOurClient(t *testing.T) {
	cfg := client.DefaultTransportConfig().WithHost("localhost:8080")
	c := client.NewHTTPClientWithConfig(nil, cfg)

	params := products.NewListProductsParams()
	val, err := c.Products.ListProducts(params)

	assert.NilError(t, err)
	for _, p := range val.GetPayload() {
		fmt.Printf("%#+v\n", p)
	}
}
