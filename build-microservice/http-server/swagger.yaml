basePath: /
consumes:
    - application/json
definitions:
    GenericError:
        description: GenericError GenericError is a generic error message returned by a server
        properties:
            message:
                description: message
                type: string
                x-go-name: Message
        type: object
        x-go-package: http-server/models
    Product:
        properties:
            description:
                description: desc
                type: string
                x-go-name: Desc
            id:
                description: ID
                format: int64
                type: integer
                x-go-name: ID
            name:
                description: the name for this product
                type: string
                x-go-name: Name
            price:
                description: the price for the product
                format: double
                maximum: 1000
                minimum: 0.01
                type: number
                x-go-name: Price
            sku:
                description: the SKU for this product
                pattern: '[a-z]+-[a-z]+-[a-z]+'
                type: string
                x-go-name: SKU
        required:
            - name
            - price
            - sku
        type: object
        x-go-package: http-server/data
    ValidationError:
        description: ValidationError ValidationError is a collection of validation error messages
        properties:
            messages:
                description: messages
                items:
                    type: string
                type: array
                x-go-name: Messages
        type: object
        x-go-package: http-server/models
info:
    description: Documentation for Product API
    title: of Product API
    version: 1.0.0
paths:
    /product:
        post:
            description: Create a new product
            operationId: createProduct
            parameters:
                - in: body
                  name: Body
                  required: true
                  schema:
                    $ref: '#/definitions/Product'
            responses:
                "201":
                    $ref: '#/responses/noContent'
                "422":
                    $ref: '#/responses/errorValidation'
                "501":
                    $ref: '#/responses/errorResponse'
            tags:
                - product
    /product/{id}:
        delete:
            description: Returns a list of products
            operationId: deleteProduct
            parameters:
                - description: The id of the product from the database
                  format: int64
                  in: path
                  name: id
                  required: true
                  type: integer
                  x-go-name: ID
            responses:
                "201":
                    $ref: '#/responses/noContent'
            tags:
                - products
        get:
            description: Returns a product
            operationId: getProduct
            parameters:
                - description: The currency for the product
                  in: query
                  name: currency
                  type: string
                  x-go-name: Currency
                - description: The id of the product from the database
                  format: int64
                  in: path
                  name: id
                  required: true
                  type: integer
                  x-go-name: ID
            responses:
                "201":
                    $ref: '#/responses/productsResponse'
            tags:
                - product
        put:
            description: update a product
            operationId: updateProduct
            parameters:
                - description: The id of the product from the database
                  format: int64
                  in: path
                  name: id
                  required: true
                  type: integer
                  x-go-name: ID
                - in: body
                  name: Body
                  required: true
                  schema:
                    $ref: '#/definitions/Product'
            responses:
                "201":
                    $ref: '#/responses/noContent'
                "422":
                    $ref: '#/responses/errorValidation'
                "501":
                    $ref: '#/responses/errorResponse'
            tags:
                - product
    /products:
        get:
            description: Returns a list of products
            operationId: listProducts
            parameters:
                - description: The currency for the product
                  in: query
                  name: currency
                  type: string
                  x-go-name: Currency
            responses:
                "200":
                    $ref: '#/responses/productsResponse'
            tags:
                - products
produces:
    - application/json
responses:
    errorResponse:
        description: Generic error message returned as a string
        schema:
            $ref: '#/definitions/GenericError'
    errorValidation:
        description: Validation errors defined as an array of strings
        schema:
            $ref: '#/definitions/ValidationError'
    noContent:
        description: No content is returned by this API endpoint
    productResponse:
        description: Data structure representing a single product
        schema:
            $ref: '#/definitions/Product'
    productsResponse:
        description: A list of products returns in the response
        schema:
            items:
                $ref: '#/definitions/Product'
            type: array
schemes:
    - http
swagger: "2.0"
