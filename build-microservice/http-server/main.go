package main

import (
	"context"
	"http-server/data"
	"http-server/files"
	"http-server/handlers"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-openapi/runtime/middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	protosCurrency "grpc-server/protos/gen/currency"

	gorillaHandler "github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {

	l := log.New(os.Stdout, "[my-svr] ", log.LstdFlags)

	///////
	conn, errDial := grpc.NewClient("localhost:9090", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if errDial != nil {
		l.Fatal(errDial)
	}
	defer conn.Close()

	currencyClient := protosCurrency.NewCurrencyClient(conn)
	///////

	h := handlers.NewHello(l)
	db := data.NewProductsDB(l, currencyClient)
	p := handlers.NewProducts(l, db)

	gzipMiddleware := handlers.NewGzipMiddleware()

	fStore, err := files.NewHostFileSystemHandler(os.TempDir())
	if err != nil {
		l.Fatal("failed to inialize the fstore", "err", err)
	}
	fs := handlers.NewFileServer(l, fStore)

	stdMux := http.NewServeMux()
	stdMux.Handle("GET /{$}", h)

	gorillaMux := mux.NewRouter()
	getRouter := gorillaMux.Methods(http.MethodGet).Subrouter()

	{
		// adding gzip middleware
		getRouter.Use(gzipMiddleware.GetMiddleware)
	}

	postRouter := gorillaMux.Methods(http.MethodPost).Subrouter()
	putRouter := gorillaMux.Methods(http.MethodPut).Subrouter()
	deleteRouter := gorillaMux.Methods(http.MethodDelete).Subrouter()

	productsEndPoints := func() {
		postRouter.HandleFunc("/products", p.AddProducts)
		postRouter.Use(p.MiddlewareProductValidation)

		putRouter.HandleFunc("/product/{id:[0-9]+}", p.UpdateProduct)
		putRouter.Use(p.MiddlewareProductValidation)

		getRouter.HandleFunc("/products", p.GetProducts).Queries("currency", "{[A-Z]{3}}")
		getRouter.HandleFunc("/products", p.GetProducts)
		getRouter.HandleFunc("/product/{id:[0-9]+}", p.GetProduct).Queries("currency", "{[A-Z]{3}}")
		getRouter.HandleFunc("/product/{id:[0-9]+}", p.GetProduct)

		deleteRouter.HandleFunc("/product/{id:[0-9]+}", p.DeleteProduct)
	}

	swaggerUIEndpoints := func() {
		sh := middleware.Redoc(middleware.RedocOpts{SpecURL: "/swagger.yaml"}, nil)
		getRouter.Handle("/docs", sh)

		getRouter.Handle("/swagger.yaml", http.FileServer(http.Dir("./")))
	}

	fileSystemEndpoints := func() {

		postFileSvrRouter := gorillaMux.Methods(http.MethodPost).Subrouter()
		postFileSvrRouter.HandleFunc(`/images/{id:[0-9]+}/{filename:[a-zA-Z]+\.[a-z]{3}}`, fs.HandlePostREST)

		postFileSvrRouter.HandleFunc("/data", fs.HandleMultipartForm)
		getRouter.Handle(
			`/images/{id:[0-9]+}/{filename:[a-zA-Z0-9_]+\.[a-z]{3}}`,
			http.StripPrefix(
				"/images/",
				http.FileServer(
					http.Dir(
						os.TempDir(),
					),
				),
			),
		)
	}

	productsEndPoints()
	swaggerUIEndpoints()
	fileSystemEndpoints()

	var cors = gorillaHandler.CORS(
		gorillaHandler.AllowedOrigins(
			[]string{
				"http://localhost:3000",
			},
		))
	svc := &http.Server{
		Addr: "127.0.0.1:8080",
		//Handler:      stdMux,
		ErrorLog:     l,
		Handler:      cors(gorillaMux),
		ReadTimeout:  1 * time.Second,
		WriteTimeout: 5 * time.Second,
		IdleTimeout:  120 * time.Second,
	}

	l.Printf("Listening on: %s\n", svc.Addr)

	go func() {
		if err := svc.ListenAndServe(); err != nil {
			l.Fatal(err)
		}
	}()

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	signal.Notify(sigChan, os.Kill)

	sig := <-sigChan
	l.Println("Received terminate, graceful shutdown", sig)

	tc, _ := context.WithTimeout(context.Background(), 30*time.Second)
	svc.Shutdown(tc)
}
