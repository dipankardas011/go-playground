package files

import "io"

type FileSystem interface {
	Save(path string, data io.ReadCloser) error
}
