package files

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
)

type HostFileSystem struct {
	baseDir string
}

func NewHostFileSystemHandler(mountDir string) (*HostFileSystem, error) {
	p, err := filepath.Abs(mountDir)
	if err != nil {
		return nil, err
	}
	return &HostFileSystem{
		baseDir: p,
	}, nil
}

func (h *HostFileSystem) Save(path string, data io.ReadCloser) error {
	loc := filepath.Join(h.baseDir, path)

	d := filepath.Dir(loc)
	err := os.MkdirAll(d, os.ModePerm)
	if err != nil {
		return fmt.Errorf("Unable to create directory: %w", err)
	}

	_, err = os.Stat(loc)
	if err == nil {
		err = os.Remove(loc)
		if err != nil {
			return fmt.Errorf("Unable to delete file: %w", err)
		}
	} else if !os.IsNotExist(err) {
		return fmt.Errorf("Unable to get file info: %w", err)
	}

	f, err := os.Create(loc)
	if err != nil {
		return fmt.Errorf("Unable to create file: %w", err)
	}
	defer f.Close()

	_, err = io.Copy(f, data)
	if err != nil {
		return fmt.Errorf("Unable to write to file: %w", err)
	}

	return nil
}
